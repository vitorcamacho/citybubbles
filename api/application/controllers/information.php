<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ob_start();

class Information extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('information_m');
        $this->load->model('language_m');
        $this->load->model('booking_m');
    }

    public function index() {
      $data['url'] = "information";
      $data['booking_unread'] = $this->booking_m->get_unread_booking();
      $data['informations'] = $this->get();
      $this->load->view("admin", $data);
    }

    public function get($id = null) {
      return $this->information_m->get($id);
    }

    public function delete($id) {
      $information = $this->information_m->get($id)[0];
      $this->file->delete($information['image']);
      $this->information_m->delete($id);
      redirect("/admin/information");
    }

    public function create_info() {
      $data['url'] = "information";
      $data['modal'] = "true";
      $data['modal_page'] = "create_info";
      $data['informations'] = $this->get();

      return $data;
    }

    public function edit_info($id = null) {
      $data['url'] = "information";
      $data['modal'] = "true";
      $data['modal_page'] = "edit_info";
      $data['informations'] = $this->get();
      $data['information'] = $this->get($id)[0];

      return $data;
    }

    public function create_info_submit() {
      $_POST['image'] = $this->file->upload($_FILES['image'], 'information');
      $this->information_m->create_info($_POST);
      redirect("/admin/information");
    }

    public function edit_info_submit() {
      if($image = $this->file->upload($_FILES['image'], 'information')){
        $information = $this->information_m->get($_POST['idinformation'])[0];
        $this->file->delete($information['image']);
        $_POST['image'] = $image;
      }
      $this->information_m->edit_info($_POST);
      redirect("/admin/information");
    }

}
