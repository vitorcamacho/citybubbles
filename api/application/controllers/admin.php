<?php
ob_start();

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

include_once './vendor/autoload.php';

class Admin extends CI_Controller {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('admin_m');
        $this->load->model('booking_m');
        $this->load->model('device_m');

        $this->load->model('video_m');
        $this->load->model('carousel_m');
        $this->load->model('admin_info_m');

        $this->load->model('green_point_info_has_language_m');
        $this->load->model('green_point_info_m');
        $this->load->model('green_point_m');

        $this->load->model('instructions_m');
        $this->load->model('instructions_has_language_m');

        $this->load->model('language_m');

        $this->load->model('line_m');

        $this->load->model('map_routes_has_steps_m');
        $this->load->model('map_routes_m');

        $this->load->model('photo_m');

        $this->load->model('poi_info_has_language_m');
        $this->load->model('poi_info_m');
        $this->load->model('poi_m');

        $this->load->model('pol_info_has_language_m');
        $this->load->model('pol_info_m');
        $this->load->model('pol_m');


        $this->load->model('route_has_map_routes_m');
        $this->load->model('route_has_poi_m');
        $this->load->model('route_info_has_language_m');
        $this->load->model('route_info_m');
        $this->load->model('route_m');

        $this->load->model('steps_m');
        $this->load->model('steps_has_instructions_m');

        $this->load->model('device_m');
    }

    public function index() {
        $data['pois'] = $this->poi_m->get_poi_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function pols() {
        $data['url'] = "pol";
        $data['pols'] = $this->pol_m->get_pol_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function login() {
        $user = $this->admin_m->login($_POST);
        if (count($user) != 0) {
            $this->session->set_userdata($user[0]);
            redirect("/admin", 'refresh');
        } else {
            $this->session->sess_destroy();
            echo "login errado";
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect("/admin");
    }

    public function booking() {
        $data['url'] = "booking";
        $data['booking'] = $this->booking_m->get_booking();
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function validate_booking($id) {
        $this->booking_m->read_booking($id);
        redirect("/admin/booking/");
    }

    public function delete_booking($id) {
        $this->booking_m->delete_booking($id);
        redirect("/admin/booking/");
    }

    public function create_poi() {
        $data['modal'] = "true";
        $data['modal_page'] = "create_poi";
        $data['languages'] = $this->language_m->get_language();
        $data['pois'] = $this->poi_m->get_poi_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function create_pol() {
        $data['url'] = "pol";
        $data['modal'] = "true";
        $data['modal_page'] = "create_pol";
        $data['languages'] = $this->language_m->get_language();
        $data['pols'] = $this->pol_m->get_pol_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function gpoints() {
        $data['url'] = "gpoints";
        $data['gpoints'] = $this->green_point_m->get_green_points();
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function create_gpoint_submit() {
        $this->green_point_m->insert_green_point($_POST);
    }

    public function delete_gpoint($id) {
        $this->green_point_m->delete_green_point($id);
        redirect('/admin/gpoints');
    }

    public function routes() {
        $data['url'] = "routes";
        $data['routes'] = $this->route_m->get_route_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function create_route() {
        $data['url'] = "routes";
        $data['modal'] = "true";
        $data['modal_page'] = "create_route";
        $data['languages'] = $this->language_m->get_language();
        $data['routes'] = $this->route_m->get_route_lang(null, 1);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function draw_route($id) {
        $data['url'] = "route_create";
        $data['all_pois'] = $this->poi_m->get_poi_lang(null, 1);
        $data['route_pois'] = $this->poi_m->get_poi_route_lang($id, 1); // 1- PT
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function download($idroute, $name) {
      $lines = $this->route_m->get_route_lat($idroute);
      $decoded = array();
      $reference = $idroute."-".$name;
      $string = array('<?xml version="1.0"?>',
        '<gpx creator="GPS Visualizer http://www.gpsvisualizer.com/" version="1.1" xmlns="http://www.topografix.com/GPX/1/1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">',
        '<trk>',
        '<name>'.$reference.'</name>',
        '<trkseg>');

      foreach ($lines as $line) {
        $pairs = Polyline::pair(Polyline::decode($line['overview_polyline']));
        foreach ($pairs as $points) {
          array_push($string, '<trkpt lat="'.$points[0].'" lon="'.$points[1].'"></trkpt>');
        }
      }

      array_push($string, '</trkseg></trk></gpx>');

      $temp = tmpfile();
      $content = implode('', $string);
      header('Content-Type: application/gpx');
      header('Content-Disposition: attachment; filename="'.$reference.'".gpx');
      header('Pragma: no-cache');
      echo $content;

      fclose($temp); // this removes the file

      redirect('/admin/routes/');
    }



    public function delete_route($idroute) {

        foreach ($this->map_routes_m->get_map_routes_instructions($idroute) as $instructions) {
            $this->instructions_m->delete_instructions_id($instructions['idinstructions']);
        }

        foreach ($this->map_routes_m->get_map_routes_steps($idroute) as $steps) {
            $this->steps_m->delete_steps_id($steps['idsteps']);
        }

        foreach ($this->map_routes_m->get_map_routes_maproutes($idroute) as $map_routes) {
            $this->map_routes_m->delete_map_routes_id($map_routes['idmap_routes']);
        }

        $this->route_m->delete_route($idroute);

        redirect('/admin/routes/');
    }

    public function draw_route_submit() {
        $idroute = $_POST['idroute'];

        foreach ($this->map_routes_m->get_map_routes_instructions($idroute) as $instructions) {
            $this->instructions_m->delete_instructions_id($instructions['idinstructions']);
        }

        foreach ($this->map_routes_m->get_map_routes_steps($idroute) as $steps) {
            $this->steps_m->delete_steps_id($steps['idsteps']);
        }

        foreach ($this->map_routes_m->get_map_routes_maproutes($idroute) as $map_routes) {
            $this->map_routes_m->delete_map_routes_id($map_routes['idmap_routes']);
        }

        $this->route_has_poi_m->delete_route_has_poi($_POST['idroute']);
        foreach ($_POST['route_points'] as $element) {
            if ($element['idpoi'] != '-1') {
                if (count($this->route_has_poi_m->get_route_has_poi($element)) == 0)
                    $this->route_has_poi_m->insert_route_has_poi($element);
            }
        }
        $this->line_m->remove_route_lines($_POST['idroute']);
        foreach ($_POST['route_lines'] as $element) {
            $this->line_m->insert_line($element);
        }
    }

    public function draw_map_route_submit() {
        $idroute = $_POST['idroute'];


        print_r($_POST);
        echo "#####";

        $map_routes = $_POST['map_route'];
        if (sizeof($this->uap_routes_m->get_map_routes($map_routes, $idroute)) == 0) {
            $idmap_route = $this->map_routes_m->insert_map_routes($map_routes);
            $this->route_has_map_routes_m->insert_route_has_map_routes($idroute, $idmap_route);

            foreach ($map_routes['step'] as $step) {
                if (sizeof($this->steps_m->get_steps($step)) == 0) {
                    $idstep = $this->steps_m->insert_steps($step);
                    $this->map_routes_has_steps_m->insert_map_routes_has_steps($idmap_route, $idstep);

                    $idinstructions = $this->instructions_m->insert_instructions($step);
                    $this->steps_has_instructions_m->insert_steps_has_instructions($idstep, $idinstructions);
                    $data = array();
                    $data['idinstructions'] = $idinstructions;
                    $data['idlanguage'] = 1;
                    $this->instructions_has_language_m->insert_instructions_has_language($data);

                    $num_languages = $this->language_m->get_language();
                    foreach ($num_languages as $languages) {
                        if ($languages['name'] != "PT") {
                            $data = array();
                            $idinstructions = $this->instructions_m->insert_empty_instructions();
                            $this->steps_has_instructions_m->insert_steps_has_instructions($idstep, $idinstructions);

                            $data['idinstructions'] = $idinstructions;
                            $data['idlanguage'] = $languages['idlanguage'];

                            $this->instructions_has_language_m->insert_instructions_has_language($data);
                        }
                    }
                }
            }
        }
    }

    public function create_poi_submit() {
        $idpoi = $this->poi_m->insert_poi($_POST);
        $idlanguage = $this->language_m->get_portuguese_language()[0]['idlanguage'];
        $_POST['idpoi'] = $idpoi;
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/poi/' . $idpoi . "." . $image_type[1];
            move_uploaded_file($_FILES['image'] ['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->poi_m->insert_image($_POST);
        }

        $idpoi_info = $this->poi_info_m->insert_poi_info($_POST);

        $_POST['idlanguage'] = $idlanguage;
        $_POST['idpoi_info'] = $idpoi_info;
        $this->poi_info_has_language_m->insert_poi_info_has_language($_POST);

        $num_languages = $this->language_m->get_language();

        foreach ($num_languages as $languages) {
            if ($languages['name'] != "PT") {
                $data = array();
                $data['idpoi'] = $_POST['idpoi'];

                $idpoi_info = $this->poi_info_m->insert_empty_poi_info($_POST);

                $data['idpoi_info'] = $idpoi_info;
                $data['idlanguage'] = $languages['idlanguage'];

                $this->poi_info_has_language_m->insert_poi_info_has_language($data);
            }
        }
        redirect('/admin');
    }

    public function create_pol_submit() {
        $idpol = $this->pol_m->insert_pol($_POST);
        $idlanguage = $this->language_m->get_portuguese_language()[0]['idlanguage'];
        $_POST['idpol'] = $idpol;
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/pol/' . $idpol . "." . $image_type[1];
            move_uploaded_file($_FILES['image'] ['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->pol_m->insert_image($_POST);
        }

        $idpol_info = $this->pol_info_m->insert_pol_info($_POST);

        $_POST['idlanguage'] = $idlanguage;
        $_POST['idpol_info'] = $idpol_info;
        $this->pol_info_has_language_m->insert_pol_info_has_language($_POST);

        $num_languages = $this->language_m->get_language();

        foreach ($num_languages as $languages) {
            if ($languages['name'] != "PT") {
                $data = array();
                $data['idpol'] = $_POST['idpol'];

                $idpol_info = $this->pol_info_m->insert_empty_pol_info($_POST);

                $data['idpol_info'] = $idpol_info;
                $data['idlanguage'] = $languages['idlanguage'];

                $this->pol_info_has_language_m->insert_pol_info_has_language($data);
            }
        }
        redirect('/admin/pols');
    }

    public function create_route_submit() {
        $idroute = $this->route_m->insert_route($_POST);
        $idlanguage = $this->language_m->get_portuguese_language()[0]['idlanguage'];
        $_POST['idroute'] = $idroute;
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/routes/' . $idroute . "." . $image_type[1];
            move_uploaded_file($_FILES['image']['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->route_m->insert_image($_POST);
        }

        $idroute_info = $this->route_info_m->insert_route_info($_POST);

        $_POST['idlanguage'] = $idlanguage;
        $_POST['idroute_info'] = $idroute_info;
        $this->route_info_has_language_m->insert_route_info_has_language($_POST);

        $num_languages = $this->language_m->get_language();

        foreach ($num_languages as $languages) {
            if ($languages['name'] != "PT") {
                $data = array();
                $data['idroute'] = $_POST['idroute'];

                $idroute_info = $this->route_info_m->insert_empty_route_info($_POST);

                $data['idroute_info'] = $idroute_info;
                $data['idlanguage'] = $languages['idlanguage'];

                $this->route_info_has_language_m->insert_route_info_has_language($data);
            }
        }

        redirect('/admin/routes/');
    }

    public function edit_poi_submit() {
        $this->poi_m->update_poi($_POST);
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/poi/' . $_POST['idpoi'] . "." . $image_type[1];
            move_uploaded_file($_FILES['image']['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->poi_m->insert_image($_POST);
        }
        $this->poi_m->update_poi($_POST);
        $this->poi_info_m->update_poi_info($_POST);

        redirect('/admin/');
    }

    public function edit_pol_submit() {
        $this->pol_m->update_pol($_POST);
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/pol/' . $_POST['idpol'] . "." . $image_type[1];
            move_uploaded_file($_FILES['image']['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->pol_m->insert_image($_POST);
        }
        $this->pol_m->update_pol($_POST);
        $this->pol_info_m->update_pol_info($_POST);

        redirect('/admin/pols');
    }

    public function edit_route_submit() {
        if ($_FILES['image']['size'] > 0) {
            $image_type = explode(".", $_FILES['image']['name']);
            $file_server = '/application/assets/img/routes/' . $_POST['idroute'] . "." . $image_type[1];
            move_uploaded_file($_FILES['image']['tmp_name'], $file_server);
            $_POST['image'] = $file_server;
            $this->route_m->insert_image($_POST);
        }

        $this->route_info_m->update_route_info($_POST);

        redirect('/admin/routes/');
    }

    public function edit_gpoint_submit() {
        print_r($_POST);
        $this->green_point_m->update_gpoint($_POST);

        redirect('/admin/gpoints');
    }

    public function update_poi_info() {
        $this->poi_info_m->update_poi_info($_POST);
    }

    public function update_pol_info() {
        $this->pol_info_m->update_pol_info($_POST);
    }

    public function update_gpoint_info() {
        print_r($_POST);
        $this->green_point_info_m->update_gpoint_info($_POST);
    }

    public function update_route_info() {
        $this->route_info_m->update_route_info($_POST);
    }

    public function delete_poi($id) {
        $poi = $this->poi_m->get_poi_lang($id, null);
        if ($poi[0]['image'] != 'null') {
            unlink(ltrim($poi[0]['image'], '/'));
        }

        $_POST['idpoi'] = $id;
        $this->poi_m->delete_poi($_POST);
        unlink(ltrim($_POST['image'], '/'));
        redirect('/admin');
    }

    public function delete_pol($id) {
        $pol = $this->pol_m->get_pol_lang($id, null);
        if ($pol[0]['image'] != 'null') {
            unlink(ltrim($pol[0]['image'], '/'));
        }

        $_POST['idpol'] = $id;
        $this->pol_m->delete_pol($_POST);
        unlink(ltrim($_POST['image'], '/'));
        redirect('/admin/pols');
    }

    public function get_poi() {
        if ($this->session->userdata('username') == '') {
            echo "Don't have permission";
            return;
        }
        echo json_encode($this->poi_m->get_poi_lang(null, 1));
    }

    public function load_poi_modal($modal, $id) {
        $data['languages'] = $this->language_m->get_language();
        $data['pois'] = $this->poi_m->get_poi_lang(null, 1);
        $data['poi'] = $this->poi_m->get_poi_lang($id, null);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $data['modal'] = "true";
        $data['modal_page'] = $modal;
        $this->load->view("admin", $data);
    }

    public function load_pol_modal($modal, $id) {
        $data['languages'] = $this->language_m->get_language();
        $data['pols'] = $this->pol_m->get_pol_lang(null, 1);
        $data['pol'] = $this->pol_m->get_pol_lang($id, null);
        $data['url'] = "pol";
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $data['modal'] = "true";
        $data['modal_page'] = $modal;
        $this->load->view("admin", $data);
    }

    public function load_route_modal($modal, $id) {
        $data['languages'] = $this->language_m->get_language();
        $data['routes'] = $this->route_m->get_route_lang(null, 1);
        $data['route'] = $this->route_m->get_route($id);
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $data['modal'] = "true";
        $data['url'] = "routes";
        $data['modal_page'] = $modal;
        $this->load->view("admin", $data);
    }

    public function load_gpoint_modal($modal, $id = null) {
        $data['languages'] = $this->language_m->get_language();
        $data['gpoints'] = $this->green_point_m->get_green_points();
        $data['gpoint'] = $this->green_point_m->get_green_points($id);
        $data['gpoints_info'] = $this->green_point_info_m->get_green_points_info();
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $data['modal'] = "true";
        $data['url'] = "gpoints";
        $data['modal_page'] = $modal;
        $this->load->view("admin", $data);
    }

    public function get_route_info($id, $idlanguage) {
        $all_pois = $this->poi_m->get_poi_lang(null, 1);
        $route_pois = $this->poi_m->get_poi_route_lang($id, $idlanguage); // 1- PT
        $route_lines = $this->line_m->get_line($id);
        $data = array();

        $data['all_pois'] = $all_pois;
        $data['route_pois'] = $route_pois;
        $data['route_lines'] = $route_lines;
        echo json_encode($data);
    }

    public function cars() {
        $data['url'] = "cars";
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function get_devices() {
        echo json_encode($this->device_m->get_device());
    }

    public function photos() {
        $data['url'] = "photos";
        $data['photos'] = $this->photo_m->get_photos();
        $data['booking_unread'] = $this->booking_m->get_unread_booking();
        $this->load->view("admin", $data);
    }

    public function upload_photo_album() {
        if ($_FILES['image']) {
          $this->photo_m->insert_photo('/'.$this->file->upload($_FILES['image'], 'album'));
        }

        redirect("/admin/photos");
    }

    public function delete_photo($idphoto) {
        $photo = $this->photo_m->get_photos($idphoto)[0];
        if ($photo['url'] != 'null') {
            $this->file->delete($photo['url']);
            $this->photo_m->delete_photo($idphoto);
        }
        redirect("/admin/photos");
    }

    public function get_gpoints() {
        echo json_encode($this->green_point_m->get_green_points());
    }

    public function get_route_lat($idroute) {
        echo json_encode($this->route_m->get_route_lat($idroute));
    }

    function update_device() {
      $data['latitude'] = 32.638066;
      $data['longitude'] = -16.929773;
      $data['date'] = "";
      $data['iddevice'] = null;
      $this->device_m->update_device($data);
    }

    //
    // VIDEOS SECTION
    //
    public function videos() {
      $data['url'] = "videos";
      $data['booking_unread'] = $this->booking_m->get_unread_booking();
      $data['videos'] = $this->video_m->get();
      $this->load->view("admin", $data);
    }

    public function insert_video() {
      $this->video_m->insert_video($this->input->post('videoId'));
      redirect("/admin/videos");
    }

    public function delete_video($id) {
      $this->video_m->delete_video($id);
      redirect("/admin/videos");
    }

    public function modal($context, $route, $id = null) {
      $this->load->library('../controllers/'.$context);
      $data['url'] = $context;
      $data['modal'] = "true";
      $data['modal_page'] = $route;
      $data['booking_unread'] = $this->booking_m->get_unread_booking();
      $data['languages'] = $this->language_m->get_language();
      if($id)
        $data = array_merge($data, $this->$context->$route($id));
      else
        $data = array_merge($data, $this->$context->$route());

      $this->load->view("admin", $data);
    }
}
