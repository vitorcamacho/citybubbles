<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;




class Api extends RestController {

    function __construct()
    {
        parent::__construct();
        $this->load->model('booking_m');
        $this->load->model('photo_m');
        $this->load->model('video_m');
        $this->load->model('slider_m');
        $this->load->model('reviews_m');
        $this->load->model('information_m');

        $this->bookingRules = array(
            array(
                    'field' => 'begin',
                    'label' => 'begin',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'begin_date',
                    'label' => 'begin_date',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'end_date',
                    'label' => 'end_date',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'end',
                    'label' => 'end',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'first_name',
                    'label' => 'first_name',
                    'rules' => 'required',
            ),
            array(
                    'field' => 'last_name',
                    'label' => 'last_name',
                    'rules' => 'required',
            ),
            array(
                    'field' => 'age',
                    'label' => 'age',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'contact',
                    'label' => 'contact',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'email',
                    'label' => 'email',
                    'rules' => 'required'
            ),
            array(
                    'field' => 'num_pass',
                    'label' => 'num_pass',
                    'rules' => 'required'
            ),
        );
    }

    public function photos_get(){
        $this->response( $this->photo_m->get_photos(), 200 );
    }

    public function videos_get(){
        $this->response( $this->video_m->get(), 200 );
    }

    public function sliders_get(){
        $this->response( $this->slider_m->get_by_language(), 200 );
    }

    public function reviews_get(){
        $this->response( $this->reviews_m->get_by_language(), 200 );
    }

    public function informations_get(){
        $this->response( $this->information_m->get_by_language(), 200 );
    }

    public function translations_get($lang = 'pt'){
        $file = APPPATH."assets/rbundle-".$lang.".json";
        $this->response( json_decode(file_get_contents($file, true)), 200 );
    }

    public function booking_post(){
        $this->form_validation->set_rules($this->bookingRules);

        if ($this->form_validation->run())
        {   
            $data['book'] = $this->input->post();
            $this->booking_m->insert_booking($this->input->post());

            $config = array (
                'mailtype' => 'html',
                'charset'  => 'utf-8',
                'priority' => '1'
            );
            $this->email->initialize($config);
            $this->email->from('info@citybubbles.pt', 'Info Citybubbles');
            $this->email->to('info@citybubbles.pt');
            $this->email->subject('Reserva - '.$this->input->post('begin_date'));
            $html = $this->load->view("email/admin_booking", $data, TRUE);

            $this->response(null, 200);
        }
        else
        {
            $data['error']  = true;
            $data['message']  = validation_errors();
            $this->response($data, 500);
        }
    }

    
}