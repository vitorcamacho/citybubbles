<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Translate extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
                
        $this->load->model('instructions_m');
        $this->load->model('instructions_has_language_m');
        
        $this->load->model('language_m');
        
        $this->load->model('line_m');
        
        $this->load->model('map_routes_has_steps_m');
        $this->load->model('map_routes_m');
        
        $this->load->model('route_has_map_routes_m');
        
        $this->load->model('steps_m');
        $this->load->model('steps_has_instructions_m');
    }
    
    
    public function index()
    {
        
    }
    
    public function instructions($idroute, $idlanguage)
    {
        $this->load->view("map/map_".$idlanguage);
    }
    
    
    public function update_instructions($idlanguage){
        $map_routes = $_POST['map_route'];
        $map_routes_array = $this->map_routes_m->get_map_routes_id($map_routes, $_POST['idroute']);
        foreach ($map_routes_array as $value) {
            foreach ($map_routes['step'] as $step) {
                 $steps_array = $this->steps_m->get_map_routes_has_steps($value['idmap_routes']);
                 foreach ($steps_array as $steps_array_value) {
                     if($step['start_latitude'] == $steps_array_value['start_latitude'] && $step['start_longitude'] == $steps_array_value['start_longitude'] && $step['end_latitude'] == $steps_array_value['end_latitude'] && $step['end_longitude'] == $steps_array_value['end_longitude'])
                     {
                         $all_idinstructions = $this->instructions_m->get_instructions($steps_array_value['idsteps'], $idlanguage);
                         foreach ($all_idinstructions as $idinstructions) {
                            $data = array();
                            $data['idinstructions'] = $idinstructions['idinstructions'];
                            $data['text'] = $step['instructions'];
                            echo $step['instructions'];
                            $this->instructions_m->update_instructions($data);
                         }
                     }
                 }
            }
        }
    }
    
}
