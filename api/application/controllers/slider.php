<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();

class Slider extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('slider_m');
        $this->load->model('language_m');
        $this->load->model('booking_m');
    }

    public function index() {
      $data['url'] = "slider";
      $data['booking_unread'] = $this->booking_m->get_unread_booking();
      $data['sliders'] = $this->get();
      $this->load->view("admin", $data);
    }

    public function get($id = null) {
      return $this->slider_m->get($id);
    }

    public function delete($id) {
      $slider = $this->slider_m->get($id)[0];
      $this->file->delete($slider['image']);
      $this->slider_m->delete($id);
      redirect("/admin/slider");
    }

    public function create_slider() {
      $data['url'] = "slider";
      $data['modal'] = "true";
      $data['modal_page'] = "create_slider";
      $data['sliders'] = $this->get();

      return $data;
    }

    public function edit_slider($id = null) {
      $data['url'] = "slider";
      $data['modal'] = "true";
      $data['modal_page'] = "edit_slider";
      $data['sliders'] = $this->get();
      $data['slider'] = $this->get($id)[0];

      return $data;
    }

    public function create_slider_submit() {
      $_POST['image'] = $this->file->upload($_FILES['image'], 'slider');
      $this->slider_m->create_slider($_POST);
      redirect("/admin/slider");
    }

    public function edit_slider_submit() {
      if($image = $this->file->upload($_FILES['image'], 'slider')){
        $slider = $this->slider_m->get($_POST['idslider'])[0];
        $this->file->delete($slider['image']);
        $_POST['image'] = $image;
      }
      $this->slider_m->edit_slider($_POST);
      redirect("/admin/slider");
    }
}
