<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

ob_start();
ini_set('memory_limit', '-1');
include(APPPATH.'libraries/cache.class.php');


class Home extends CI_Controller {

    function __construct()
    {
        parent::__construct();// you have missed this line.
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->library('email');
        $this->load->model('booking_m');
        $this->load->model('poi_m');
        $this->load->model('route_m');
        $this->load->model('line_m');
        $this->load->model('map_routes_m');
        $this->load->model('steps_m');
        $this->load->model('instructions_m');
        $this->load->model('photo_m');
        $this->load->model('video_m');
        $this->load->model('slider_m');
        $this->load->model('reviews_m');
        $this->load->model('information_m');
    }

    public function index()
    {
      $data['lang'] = isset($this->session->userdata['lang']) ? $this->session->userdata['lang'] : "pt";
      $idlanguage = ($data['lang'] == 'pt' ? 1 : 2);
      $data['photos'] = $this->get_photos();
      $data['rbundle'] = $this->loadRbundle($data['lang']);
      $data['videos'] = $this->video_m->get();
      $data['sliders'] = $this->slider_m->get_by_language();
      $data['reviews'] = $this->reviews_m->get_by_language();
      $data['informations'] = $this->information_m->get_by_language();

      $this->load->view("home", $data);
    }

    public function checkCache($key, $data) {
      echo $key;
      print_r($this->cache->retrieve($key));
      if($this->cache->retrieve($key) !== null){
        echo "store cache";
        return $this->cache->retrieve($key);
      }
      else {
        echo "get stored data";
        $this->cache->store($key, $data);
        return $data;
      }
    }

    public function loadRbundle($lang) {
      $file = APPPATH."assets/rbundle-".$lang.".json";
      return json_decode(file_get_contents($file, true));
    }

    public function lang($lang)
    {
        $this->session->set_userdata("lang", $lang);
        redirect('/');
    }

    public function booking(){
        foreach ($_POST as $key => $value) {
            if($value == "" || !isset($value)){
                echo "ERROR: One or more fields empty.<br>ERRO: Um ou mais campos vazios.";
                return;
            }
        }

        $data['book'] = $_POST;
        $this->booking_m->insert_booking($_POST);

        //email send
        $config = array (
                  'mailtype' => 'html',
                  'charset'  => 'utf-8',
                  'priority' => '1'
                   );
        $this->email->initialize($config);
        $this->email->from('info@citybubbles.pt', 'Info Citybubbles');
        $this->email->to('info@citybubbles.pt');
        $this->email->subject('Reserva - '.$_POST['begin_date']);
        $html = $this->load->view("email/admin_booking", $data, TRUE);
        $this->email->message($html);
        $this->email->send();
        redirect('/');
    }

    public function get_photos(){
        return $this->photo_m->get_photos();
    }

    public function get_routes($idlanguage){
        $route_aux = array();
        $routes = $this->route_m->get_route_lang(null,$idlanguage);
        foreach ($routes as $key=>$route) {
                $route['map_routes'] = $this->map_routes_m->get_map_routes_maproutes($route['idroute']);
                $route['poi'] = $this->poi_m->get_poi_route_lang($route['idroute'], $idlanguage);
                foreach ($route['map_routes'] as $key2=>$map_routes) {
                    $map_routes['steps'] = $this->steps_m->get_map_routes_has_steps($map_routes['idmap_routes']);
                    $route['map_routes'][$key2]['steps'] = $map_routes['steps'];
                    foreach ($map_routes['steps'] as $key3=>$step) {
                        $step['intructions'] = $this->instructions_m->get_instructions($step['idsteps'], $idlanguage);
                        $route['map_routes'][$key2]['steps'][$key3]['instructions'] = $step['intructions'];
                    }
                }
                array_push($route_aux ,$route);
        }
        echo json_encode($route_aux);
    }

}
