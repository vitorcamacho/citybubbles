<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");

class App extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');


        $this->load->model('admin_m');

        $this->load->model('booking_m');

        $this->load->model('device_m');

        $this->load->model('green_point_info_has_language_m');
        $this->load->model('green_point_info_m');
        $this->load->model('green_point_m');

        $this->load->model('instructions_m');
        $this->load->model('instructions_has_language_m');

        $this->load->model('language_m');

        $this->load->model('line_m');

        $this->load->model('map_routes_has_steps_m');
        $this->load->model('map_routes_m');

        $this->load->model('poi_info_has_language_m');
        $this->load->model('poi_info_m');
        $this->load->model('poi_m');

        $this->load->model('pol_info_has_language_m');
        $this->load->model('pol_info_m');
        $this->load->model('pol_m');


        $this->load->model('route_has_map_routes_m');
        $this->load->model('route_has_poi_m');
        $this->load->model('route_info_has_language_m');
        $this->load->model('route_info_m');
        $this->load->model('route_m');

        $this->load->model('steps_m');
        $this->load->model('steps_has_instructions_m');
    }

    function get_languages() {
        $languages = $this->language_m->get_language();
        echo json_encode($languages);
    }

    function get_route_languages() {
        $languages_aux = array();
        $languages = $this->language_m->get_language();
        foreach ($languages as $language) {
            $routes_languages = $this->route_m->get_route_lang(null, $language['idlanguage']);
            array_push($languages_aux, $routes_languages);
        }
        echo json_encode($languages_aux);
    }

    function get_route_pois() {
        $data = array();
        $routes_aux = array();
        $languages_aux = array();
        $languages = $this->language_m->get_language();
        $routes = $this->route_m->get_routes();
        foreach ($languages as $language) {
            foreach ($routes as $key => $route) {
                $route_pois = $this->poi_m->get_poi_route_lang($route['idroute'], $language['idlanguage']);
                $route_pois_aux = array();
                foreach ($route_pois as $poi) {
                    $poi['read'] = 0;
                    array_push($route_pois_aux, $poi);
                }
                $routes_aux[$route['idroute']] = $route_pois_aux;
            }
            array_push($languages_aux, $routes_aux);
        }
        echo json_encode($languages_aux);
    }

    function get_pols() {
        $data = array();
        $routes_aux = array();
        $languages_aux = array();
        $languages = $this->language_m->get_language();
        $pols = $this->pol_m->get_pol();
        foreach ($languages as $language) {
            foreach ($pols as $key => $pol) {
                $all_pois = $this->pol_m->get_pol_lang($pol['idpol'], $language['idlanguage']);
                $pols_aux[$pol['idpol']] = $all_pois;
            }
            array_push($languages_aux, $pols_aux);
        }
        echo json_encode($languages_aux);
    }

    function get_route_gpoints() {
        $points = $this->green_point_m->get_green_points();
        $languages = $this->language_m->get_language();
        $languages_aux = array();
        foreach ($languages as $language) {
            foreach ($points as $key => $point) {
                $text = $this->green_point_info_m->get_green_points_info($language['idlanguage']);
                $points[$key]['instructions'] = $text[0]['text'];
            }
            array_push($languages_aux, $points);
        }
        echo json_encode($languages_aux);
    }

    function get_map_route() {
        $route_aux = array();
        $languages_aux = array();
        $data = array();
        $data['routes'] = $this->route_m->get_routes();
        $route_languages = array();
        $languages = $this->language_m->get_language();

        foreach ($languages as $language) {
            foreach ($data['routes'] as $key => $route) {
                $route['map_routes'] = $this->map_routes_m->get_map_routes_maproutes($route['idroute']);
                foreach ($route['map_routes'] as $key2 => $map_routes) {
                    $map_routes['steps'] = $this->steps_m->get_map_routes_has_steps($map_routes['idmap_routes']);
                    $route['map_routes'][$key2]['steps'] = $map_routes['steps'];
                    foreach ($map_routes['steps'] as $key3 => $step) {
                        $step['intructions'] = $this->instructions_m->get_instructions($step['idsteps'], $language['idlanguage']);
                        $route['map_routes'][$key2]['steps'][$key3]['instructions'] = $step['intructions'];
                    }
                }
                array_push($route_aux, $route);
            }
            array_push($languages_aux, $route_aux);
            $route_aux = array();
        }
        echo json_encode($languages_aux);
    }

    function get_route_lines($idroute) {
        $route_lines = $this->line_m->get_line($idroute);
        echo json_encode($route_lines);
    }

    function regist_device($iddevice) {
        $device = $this->device_m->get_device($iddevice);
        echo sizeof($device);
        if (sizeof($device) == 0)
            $this->device_m->insert_device($iddevice);
    }

    function update_device() {
        $this->device_m->update_device($_POST);
    }

    public function get_hour() {
        echo now();
    }

}
