<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
ob_start();

class Reviews extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('reviews_m');
        $this->load->model('language_m');
        $this->load->model('booking_m');
    }

    public function index() {
      $data['url'] = "reviews";
      $data['booking_unread'] = $this->booking_m->get_unread_booking();
      $data['reviews'] = $this->get();

      $this->load->view("admin", $data);
    }

    public function get($id = null) {
      return $this->reviews_m->get($id);
    }

    public function delete($id) {
      $this->reviews_m->delete($id);
      redirect("/admin/reviews");
    }

    public function create_reviews() {
      $data['url'] = "reviews";
      $data['modal'] = "true";
      $data['modal_page'] = "create_reviews";
      $data['reviews'] = $this->get();

      return $data;
    }

    public function edit_reviews($id = null) {
      $data['url'] = "reviews";
      $data['modal'] = "true";
      $data['modal_page'] = "edit_reviews";
      $data['reviews'] = $this->get();
      $data['review'] = $this->get($id)[0];

      return $data;
    }

    public function create_reviews_submit() {
      $this->reviews_m->create_reviews($_POST);
      redirect("/admin/reviews");
    }

    public function edit_reviews_submit() {
      $this->reviews_m->edit_reviews($_POST);
      redirect("/admin/reviews");
    }
}
