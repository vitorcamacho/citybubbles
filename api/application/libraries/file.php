<?php

class File {
  
  public function upload($file, $target) {
    if($file && !$file['error']) {
      $path = APPPATH.'assets/uploads/'.$target.'/';
      $filename = hash('md5', $file['name']).".".rand (1,100000000).rand (1,100000000).'.'.explode('.', $file['name'])[1];
      move_uploaded_file($file['tmp_name'], $path.$filename);
      return '/'.$path.$filename;
    } else {
      return null;
    }
  }

  public function delete($file) {
    unlink(ltrim($file, '/'));
  }
}
