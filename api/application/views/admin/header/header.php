<nav class="navbar navbar-default navbar-fixed-top " role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo "/admin"?>">Admin Page</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li>
                 <a href="<?php echo "/admin"?>">Pontos de Interesse</a>
            </li>
            <li>
                 <a href="<?php echo "/admin/pols"?>">Pontos de Localização</a>
            </li>
            <li>
                 <a href="<?php echo "/admin/gpoints"?>">Pontos de Carga</a>
            </li>
            <li>
                <a href="<?php echo "/admin/routes"?>">Rotas</a>
            </li>
            <!-- <li>
                <a href="<?php echo "/admin/cars"?>">Carros</a>
            </li> -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Front-End
                <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="<?php echo "/admin/slider"?>">Slider</a></li>
                  <li><a href="<?php echo "/admin/information"?>">Info Sections</a></li>
                  <li><a href="<?php echo "/admin/reviews"?>">Reviews</a></li>
                  <li><a href="<?php echo "/admin/photos"?>">Fotos</a></li>
                  <li><a href="<?php echo "/admin/videos"?>">Videos</a></li>
                </ul>
            </li>
            <li>
                <a href="https://www.dropbox.com/s/tqzvuk50voeq21q/citybubbles.apk?dl=1">Download App</a>
            </li>
        </ul>
        <form class="navbar-form navbar-right" action="">
            <?php if(count($booking_unread) > '0'):?>
                <a href="/admin/booking/" class="btn btn-default" role="button"><span style="margin-left: 5px; margin-top: 2px" class="badge pull-right"><?php echo count($booking_unread);?></span>Reservas</a>
            <?php else:?>
                <a href="/admin/booking/" class="btn btn-default" role="button">Reservas</a>
            <?php endif;?>
            <a href="/admin/logout/" class="btn btn-danger" role="button">Logout</a>
        </form>
    </div>
  </div>
</nav>
