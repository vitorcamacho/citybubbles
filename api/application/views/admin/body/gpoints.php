<div style="margin-bottom: 5px; margin-top: 60px; float:right" class="col-lg-2" >
   <a type="button" href="<?php echo "/admin/load_gpoint_modal/translate_gpoint/";?>" class="btn btn-primary"><b>Traduzir Ponto de Carga</b></a>
</div>
<div style="margin-bottom: 5px; margin-top: 60px; float:right" class="col-lg-2" >
   <a type="button" href="<?php echo "/admin/load_gpoint_modal/create_gpoint"?>" class="btn btn-primary"><b>Criar Ponto de Carga</b></a>
</div>
    
<table class="table table-hover col-lg-12 tablesorter">
        <thead style="border: 1px solid silver; background-color: lightgrey">
            <tr>
                <th class="col-lg-1"></th>
                <th class="col-lg-1">ID</th>
                <th class="col-lg-3">Latitude</th>
                <th class="col-lg-3">Longitude</th>
                <th style="text-align: center" class="col-lg-2">Opções</th>
            </tr>
        </thead>
        <tbody style="font-size:12px;">
            <?php foreach($gpoints as $gpoint):?>
            <tr>
                <td class="col-lg-1"></td>
                <td class="col-lg-1"><?php echo $gpoint['idgreen_point']?> </td>
                <td class="col-lg-3"><?php echo $gpoint['latitude'] ?> </td>
                <td class="col-lg-4"><?php echo $gpoint['longitude'] ?> </td>
                <td class="col-lg-2" style="text-align: center">
                    <a type="button" href="<?php echo "/admin/load_gpoint_modal/edit_gpoint/".$gpoint['idgreen_point'];?>" class="btn btn-primary" title="Editar ponto de carga"> <span class="glyphicon glyphicon-cog"></span></a>
                    <a type="button" href="<?php echo "/admin/delete_gpoint/".$gpoint['idgreen_point'];?>" class="btn btn-primary" title="Eliminar ponto de carga"> <span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>


<script>
$(document).ready(function() { 
    $(".table").tablesorter(); 
}); 
</script>
