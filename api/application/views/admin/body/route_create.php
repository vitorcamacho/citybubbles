<div class="col-lg-12 " style="margin-top: 70px;">
    <div class="row col-lg-9">
            <div class="col-lg-5">
                Pontos de interesse
                <div class="radio">
                    <label>
                        <input type="radio" name="view_mode" id="all" value="all" checked>
                        Todos
                    </label>
                    <label>
                        <input type="radio" name="view_mode" id="some" value="some">
                        Apenas da rota
                    </label>
                </div>
            </div>
        <div class="loader col-lg-3" style="display: none;">
            <br>
            <img width="25" height="25" src="<?php echo "/application/assets/img/loader.gif"?>" alt="loader">
            <a class="message-loading" style="pointer-events: none; color: #000"></a>
        </div>
        <div class="col-lg-12" id="map-canvas">
        </div>
        <a style="margin-top:10px; width:100%;" tygetpe="button" href="javascript:post()" class="btn btn-primary btn-lg" title="Guardar Rota">Guardar</a>
    </div>
        
        
    
    <div class="col-lg-3" style="margin-top: -20px;">
        <table class="table table-hover" >
            <thead>
                <tr >
                    <th class="col-lg-12" colspan="4"><h3>Pontos de Interesse</h3></th>
                </tr>
            </thead>
            <tbody style="font-size:14px" class="poi_selected">
                <?php foreach ($route_pois as $poi):?>
                <tr>
                    <?php if($poi['poi_image'] != 'null'):?>
                        <td><img style="margin-left: 10px" class="img-rounded"  width="40" height="40" src="<?php echo $poi['poi_image'];?>"></td>
                    <?php else:?>   
                        <td><img style="margin-left: 10px" class="img-rounded"  width="40" height="40" src=""></td>
                    <?php endif;?>
                    <td><?php echo $poi['idpoi']." - ".$poi['poi_name'];?></td>
                </tr>
                <?php endforeach;?>
            </tbody>
        </table>
        
    </div> 
</div>
<style>
    #map-canvas {
        height: 480px;
        margin: 0px;
        padding: 0px
    }
</style>
<script>
    // GMAPS //
    var route_lines, all_pois, route_pois, all_routes, map;
    var click_route = 0, click_marker = 1;
    var route = new Array(), path = new Array();
    var view_mode = $('input[name=view_mode]:checked').val();
    var allmarkers = new Array(), poi_markers= new Array(), allpoi_markers= new Array();
    var idroute;
    
    
    map = new GMaps({
        div: '#map-canvas',
        lat: 32.640316,
        lng: -16.923928,
        zoom: 14,
        zoomControl : true,
        enabled:false,
        click: function(event){
            var myIcon = new google.maps.MarkerImage("/application/assets/img/map/dot_black.png", null, null, null, new google.maps.Size(25,25));
            var marker = new google.maps.Marker({
               icon: myIcon
            });
//            updateCoordinates(event, marker);
//            map.addMarker(marker);
            allmarkers.push(marker);
        
            create_mode(event, marker);
        }
    });  
    // END GMAPS -------------------------------------------------------//
    // -----------------------------------------------------------------//
    
    
    function resetMarkers(marker){
       
        for(var i = 0; i < allmarkers.length; i++){
            var myIcon = new google.maps.MarkerImage("/application/assets/img/map/dot_black.png", null, null, null, new google.maps.Size(25,25));
            allmarkers[i].setIcon(myIcon);
        }
        var myIcon = new google.maps.MarkerImage("/application/assets/img/map/dot_blue.png", null, null, null, new google.maps.Size(25,25));
        marker.setIcon(myIcon);
    }
    
    function create_mode(evt, marker){
        var coordinates = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
        click_route++;
        if(click_route == 1 && click_marker == 1){
            path.push(coordinates);
            updateCoordinates(coordinates, marker);
            map.addMarker(marker);
        }
        else if(click_route == 2 && click_marker == 1)
        {
            path.push(coordinates);
            var aux = {
                idroute: idroute,
                latitude1: path[0].lat(),
                longitude1: path[0].lng(),
                latitude2: path[1].lat(),
                longitude2: path[1].lng()
            };
            route_lines.push(aux);
            drawOneLine(aux,marker);
            path.shift();
            click_route = 1;
            resetMarkers(marker);
        }

        setMarkerEvent(marker);
        
    }
        
    function drawRoute(points){
        all_routes = new Array();
        $('.loader').show();
        $('.message-loading').html("Carregando mapa...");
        var index = 0;
        var length = points.length-1;
       
        if(length >= 0)
            drawLine(points, index, length);
        else
            $('.loader').hide();
    }
    
    function drawLine(route, index, length){
        map.getRoutes({
            origin: [route[index]['latitude1'], route[index]['longitude1']],
            destination: [route[index]['latitude2'], route[index]['longitude2']],
            travelMode: 'driving',
            callback: function(e){
                var coordinates = new google.maps.LatLng(route[index]['latitude2'], route[index]['longitude2']);
                var aux = e[index];
                all_routes.push(aux);
                if(index < length){
                    drawRouteLines(new Array(aux));
                    drawLine(route, ++index, length);
                }
                else if(index == length){
                    drawRouteLines(new Array(aux));
                    setPoints(all_routes);
                    $('.loader').hide();
                    return;
                }
            },
            error: function(){
                setTimeout(function() { drawLine(route, index, length); }, 0);
            }
        });
    }
    
    function drawOneLine(route, mark){
        map.getRoutes({
            origin: [route['latitude1'], route['longitude1']],
            destination: [route['latitude2'], route['longitude2']],
            travelMode: 'driving',
            callback: function(e){
//                var coordinates = new google.maps.LatLng(route['latitude2'], route['longitude2']);
                var aux = e[e.length-1];
                all_routes.push(aux);
                updateCoordinates(aux.legs[0].end_location, mark);
                map.addMarker(mark);
                
                $('.loader').hide();
                drawRouteLines(new Array(aux));
                return;
            }
        });
    }
    
    
    
    function drawRouteLines(points){
        points.forEach(function(item,key){
            var polys = google.maps.geometry.encoding.decodePath(item.overview_polyline);
            setTimeout(function(){
                var polyline_aux = map.drawPolyline({
                        path: polys,
                        strokeColor: '#131540',
                        strokeOpacity: 0.6,
                        strokeWeight: 3
                      });
                      points[key]['polyline'] = polyline_aux;
            },0);
        });
    }
    
    
    function setMarkerEvent(marker){
        google.maps.event.addListener(marker, 'click', function() {
            console.log(marker.position);
            click_route = 1;
            click_marker = 1;
            path.shift();
            path.push(marker.getPosition());
            resetMarkers(marker);
            
        });
        
        google.maps.event.addListener(marker,  'rightclick',  function() {
            if(confirm("Deseja remover este ponto?") === true)
            {
                var coordinates = new google.maps.LatLng(marker.position.lat(),marker.position.lng());
                removePoint(coordinates);
                click_route = 0;
                click_marker = 1;
            }
            else
                console.log("nao removido");      
        });
    }
    
    function removeMarker(marker){
        allmarkers.forEach(function(item){
            if(item == marker)
            {
                map.removeOverlay(item);  
            }
        });
    }
    
    function updateCoordinates(coord, marker){
        var marker_location = new google.maps.LatLng(coord.lat(),coord.lng());
        marker.setPosition(marker_location);
    }   
    
    function initialize(){
        var url = document.URL;
        var id = url.split("/");
        idroute = id[id.length-1];
        $.ajax({
            url: "/admin/get_route_info/"+idroute+"/1",
            dataType: "json",
            success: function(data){
                var info = eval(data);
                all_pois = info['all_pois'];
                route_lines = info['route_lines'];
                route_pois = info['route_pois'];
            },
            complete: function(){
                all_pois.forEach(function(point,key){
                    route_pois.forEach(function(route_point){
                        if(point['idpoi']== route_point['idpoi'])
                            all_pois.splice(key,1);
                    });
                });
                drawPoi();
                drawRoute(route_lines);
            },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
    }
    
    function setPoints(points){
         var positions = new Array();
        points.forEach(function(item){
            position1 = [item.legs[0].start_location.lat(),item.legs[0].start_location.lng()];
            if(JSON.stringify(positions).indexOf(JSON.stringify(position1)) == -1)
                positions.push(position1);
            
            position2 = [item.legs[0].end_location.lat(),item.legs[0].end_location.lng()];
            if(JSON.stringify(positions).indexOf(JSON.stringify(position2)) == -1)
                positions.push(position2);
        });  
            
        for(var i = 0; i<positions.length; i++){
                var coordinates = new google.maps.LatLng(positions[i][0],positions[i][1]);
                var myIcon = new google.maps.MarkerImage("/application/assets/img/map/dot_black.png", null, null, null, new google.maps.Size(25,25));
                var marker = new google.maps.Marker({
                   icon: myIcon,
                   position: coordinates
                });
                map.addMarker(marker);
                allmarkers.push(marker);
                setMarkerEvent(marker); 
            
        }
    }
    
    function drawPoi(){
        allpoi_markers.forEach(function(item){
           item.setMap(null); 
        });
        
        poi_markers.forEach(function(item){
           item.setMap(null);
        }); 
            
        if(view_mode == "all"){
            allpoi_markers = [];
//            drawRoutePoi();
            drawAllPoi();
        }
        else if(view_mode == "some"){
            poi_markers = [];
            drawRoutePoi();
        }
    }
    
    function drawAllPoi(){
        all_pois.forEach(function(poi,key){
                    var myIcon = new google.maps.MarkerImage('/application/assets/img/map/poi_black.png', null, null, null, new google.maps.Size(50,50));
                    var marker = map.addMarker({
                       lat: poi['latitude'],
                       lng: poi['longitude'],
                       title: poi['poi_name'],
                       icon: myIcon,
                       customInfo: poi,
                        infoWindow: {
                             content: '<div style="text-align:center">'+poi['poi_name']+'</b></div>'
                        }
                    });

                    google.maps.event.addListener(marker,  'rightclick',  function(event) {
                        if(poi['idroute'] == idroute)
                            removePoiEvent(event, marker);
                        else
                            addPoiEvent(event, marker);
                    });
                   
                    allpoi_markers.push(marker);
                    if(key == all_pois.length-1)
                        drawRoutePoi();
        });
    
    }
    
    function drawRoutePoi(){
        route_pois.forEach(function(poi,key){   
                var myIcon = new google.maps.MarkerImage('/application/assets/img/map/poi_blue.png', null, null, null, new google.maps.Size(50,50));
                var marker = map.addMarker({
                   lat: poi['latitude'],
                   lng: poi['longitude'],
                   title: poi['poi_name'],
                   icon: myIcon,
                   customInfo: poi,
                   infoWindow: {
                        content: '<div style="text-align:center">'+poi['poi_name']+'</div>'
                   }
                });
                
                google.maps.event.addListener(marker,  'rightclick',  function(event) {
                    if(marker.icon.url.indexOf('poi_blue.png') > -1)
                        removePoiEvent(event,marker);
                    else
                        addPoiEvent(event,marker);
                });
                
                poi_markers.push(marker);
            });
    
    }
    
    function removePoiEvent(event, marker){
        if(confirm("Deseja remover este ponto de interesse?") === true)
        {   
            if(view_mode == "some")
                marker.setMap(null);
            else{
                var myIcon = new google.maps.MarkerImage('/application/assets/img/map/poi_black.png', null, null, null, new google.maps.Size(50,50));
                marker.setIcon(myIcon);
            }
            removePoi(marker);
            refreshTable();
        }
    }
    
    function addPoiEvent(event, marker){
        if(confirm("Deseja adicionar este ponto de interesse?") === true)
        {
            var myIcon = new google.maps.MarkerImage('/application/assets/img/map/poi_blue.png', null, null, null, new google.maps.Size(50,50));
            marker.setIcon(myIcon);
            addPoi(marker);
            refreshTable();
        }   
    }
    
    function refreshTable(){
        
        var element = "";
        
        route_pois.forEach(function(poi){
            element += "<tr>";
            if(poi['poi_image']!= "null")
                element += "<td><img style='margin-left: 10px' class='img-rounded'  width='40' height='40' src='"+poi['poi_image']+"'></td>";
            else
                element += "<td><img style='margin-left: 10px' class='img-rounded'  width='40' height='40' src=''></td>";
            
            element += "<td>"+poi['idpoi']+" - "+poi['poi_name']+"</td></tr>";
            
        });
        
        $('.poi_selected').html(element);

    }
    
    function addPoi(mark){
        mark.customInfo['idroute'] = idroute;
        route_pois.push(mark.customInfo);
    }
    
    function removePoi(mark){
        mark.customInfo['idroute'] = null;
        var index = route_pois.indexOf(mark.customInfo);
        if (index > -1) {
            route_pois.splice(index, 1);
            all_pois.push(mark.customInfo);
        }
    }
    
    function removePoint(coord){
//        map.routes.forEach(function(item, key){
//            if(item.legs[0].end_location.lat() == coord.lat() && item.legs[0].end_location.lng() == coord.lng())
//            {
//                console.log(map.routes);
//                map.routes.splice(key, 1);
//                console.log(map.routes);
//            }
//        });
        
        all_routes.forEach(function(item, key){
            if(item.legs[0].end_location.lat() == coord.lat() && item.legs[0].end_location.lng() == coord.lng())
            {
                var poly_aux = item.polyline;
                poly_aux.setMap(null);
                all_routes.splice(key, 1);
            }
        });
        route_lines.forEach(function(item, key){
            if((item['latitude1'] == coord.lat() && item['longitude1'] == coord.lng()) || (item['latitude2'] == coord.lat() || item['longitude2'] == coord.lng()))
            {
                route_lines.splice(key, 1);
            }
        });
        allmarkers.forEach(function(item, key){
            if(item.position.lat() == coord.lat() && item.position.lng() == coord.lng())
            {
                item.setMap(null);
                allmarkers.splice(key, 1);
            }
        });
        
        path.shift();
        click_marker = 0;
    }
    
    // Controls    //
    $('input[name=view_mode]').on('change', function(e){
        view_mode = $('input[name=view_mode]:checked').val();
        drawPoi();
    });
    //--------------//
    
    $(document).ready(function(){
       initialize();
    });
    
    

</script>

<script>

    function post(a){
        $('.loader').show();
        $('.message-loading').html("Guardando mapa...");
        var poi = new Array(), lines = new Array();
        route_pois.forEach(function(item){
             poiData = {
                idpoi: item['idpoi'],
                idroute: idroute
            };
            poi.push(poiData);
        });
        all_routes.forEach(function(item){
            lineData = {
               idroute: idroute,
               latitude1: item.legs[0].start_location.lat(),
               longitude1: item.legs[0].start_location.lng(),
               latitude2: item.legs[0].end_location.lat(), 
               longitude2: item.legs[0].end_location.lng()
            };
            lines.push(lineData);
    
        });

        var all_map_routes = new Array();
        all_routes.forEach(function(routes, key2){
            map_route = {
                start_latitude : routes.legs[0].start_location.lat(),
                start_longitude : routes.legs[0].start_location.lng(),
                end_latitude : routes.legs[0].end_location.lat(),
                end_longitude : routes.legs[0].end_location.lng()
            };
            map_route['overview_polyine'] = routes.overview_polyline;
            var array_steps = new Array();
            routes.legs[0].steps.forEach(function(steps, key){
                var a = new RegExp('<b>', 'g');
                var b = new RegExp('</b>', 'g');
                var c = new RegExp('</div>', 'g');
                var aux2 = steps.instructions.replace(a,'');
                aux2 = aux2.replace(b,'');
                aux2 = aux2.replace(c,'');
                aux2 = aux2.split('<div style="font-size:0.9em">'); 
                step = {
                    start_latitude : steps.start_location.lat(),
                    start_longitude : steps.start_location.lng(), 
                    end_latitude : steps.end_location.lat(),
                    end_longitude : steps.end_location.lng(), 
                    distance : steps.distance.value,
                    time : steps.duration.value,
                    encoded_lat_lngs: steps.encoded_lat_lngs,
                    instructions: aux2[0]
                };
                array_steps.push(step);

            });
            map_route['step'] = array_steps;     
            all_map_routes.push(map_route);
        });
        postData(poi, lines, all_map_routes);
    }

    function postData(poi, lines, routes){
        $.ajax({
            type: "POST",
            url: "/admin/draw_route_submit/",
            data: {
                idroute: idroute, 
                route_points: poi,
                route_lines: lines
            },
            success: function(e){
//                console.log(e);
            },
            complete: function(e){
                var index = 0;
                var length = routes.length-1;

                if(length > 0)
                    postMapData(routes, index, length);
                else
                    window.location = "/admin/draw_route/"+idroute;
            },
            error: function (request, status, error) {
//                console.log("error - "+request.responseText);
//                console.log("error - "+status);
            }
        });
    }
    
   
    
    function postMapData(routes, index, length){
        console.log(routes[index]);
        $.ajax({
            type: "POST",
            url: "/admin/draw_map_route_submit/",
            data: {
                idroute: idroute, 
                map_route: routes[index]
            },
            success: function(e){
//                console.log(e);
            },
            complete: function(e){
                if(index < length){
                    postMapData(routes, ++index, length);
                }
                if(index == length){
                    window.location = "/translate/instructions/"+idroute+"/2";
                }
            },
            error: function(xhr, textStatus, error){
//                console.log(xhr.statusText);
//                console.log(textStatus);
//                console.log(error);
            }
        });
    }

    
   
   
   
</script>