<table class="table table-hover col-lg-12 tablesorter" style="margin-top:50px">
    <thead style="border: 1px solid silver; background-color: lightgrey">
        <tr style="text-align: center;">
            <th style="text-align: center;" class="col-lg-2">Nome</th>
            <th style="text-align: center;" class="col-lg-1">Contacto</th>
            <th style="text-align: center;" class="col-lg-2">Email</th>
            <th style="text-align: center;" class="col-lg-1">Data Início</th>
            <th style="text-align: center;" class="col-lg-1">Data Fim</th>
            <th style="text-align: center;" class="col-lg-1">Inicio</th>
            <th style="text-align: center;" class="col-lg-1">Fim</th>
            <th style="text-align: center;" class="col-lg-2">Número carros</th>
            <th style="text-align: center;" class="col-lg-1">Lido</th>
            <th style="text-align: center;" style="text-align: center" class="col-lg-1">Opções</th>
        </tr>
    </thead>
    <tbody style="font-size:13px">
        <?php foreach($booking as $book):?>
        <?php if($book['read'] == '1'):?>
            <tr style="text-align: center;">
        <?php else:?>    
            <tr class="warning" style="text-align: center;">
        <?php endif;?>
       
            <td class="col-lg-2"><?php echo $book['first_name']." ".$book['last_name'] ?> </td>
            <td class="col-lg-1"><?php echo $book['contact'] ?> </td>
            <td class="col-lg-2"><?php echo $book['email'] ?> </td>
            <td class="col-lg-1"><?php echo $book['begin_date'] ?> </td>
            <td class="col-lg-1"><?php echo $book['end_date'] ?> </td>
            <td class="col-lg-1"><?php echo $book['begin'] ?> </td>
            <td class="col-lg-1"><?php echo $book['end'] ?> </td>
            <td class="col-lg-1"><?php echo $book['num_pass'] ?> </td>
            <?php if($book['read'] == '1'):?>
                <td class="col-lg-1">Sim</td>
            <?php else:?>    
                <td class="col-lg-1">Não</td>
            <?php endif;?>
            <td class="col-lg-1" style="text-align: center">
                <?php if($book['read'] == '0'):?>
                    <a type="button" href="<?php echo "/admin/validate_booking/".$book['idbooking'];?>" class="btn btn-primary" title="Validar reserva"> <span class="glyphicon glyphicon-ok"></span></a>
                <?php endif;?>
                <a type="button" href="<?php echo "/admin/delete_booking/".$book['idbooking'];?>" class="btn btn-primary" title="Eliminar reserva"> <span class="glyphicon glyphicon-remove"></span></a>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>

<script>
$(document).ready(function(){
    $(".table").tablesorter(); 
});
</script>


