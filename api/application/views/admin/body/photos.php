<form style="margin-top:70px; margin-left: 20px" enctype="multipart/form-data" action="<?php echo "/admin/upload_photo_album"?>" method="POST" id="upload_photo">
    <div class="row">
        <div class="col-xs-4">
            <input type="file" class="form-control" name="image" id="image" required/>
        </div>
        <div class="col-xs-4">
            <input type="submit" class="btn btn-info"/>
        </div>
    </div>
</form>
<br><br>
<div class="col-lg-12">
  <?php foreach($photos as $photo):?>
  <div class="col-lg-2 imageWrapper" style="margin: 20px; position: relative; width: 100px; height: 100px;">
    <img src="<?php echo $photo['url'];?>" style="width: inherit; height: inherit; position: relative; z-index: 1;" />
    <a type="button" style="position: absolute; left:45px; top: 40px; z-index: 10;" href="<?php echo "/admin/delete_photo/".$photo['idphoto'];?>" class="btn btn-primary" title="Eliminar ponto de interesse">
      <span class="glyphicon glyphicon-remove" ></span>
    </a>
  </div>

  <?php endforeach;?>
</div>
