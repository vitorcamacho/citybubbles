<div style="margin-bottom: 10px; margin-top: 70px; float:right" class="col-lg-1" >
    <a type="button" href="<?php echo "/admin/modal/reviews/create_reviews"?>" class="btn btn-primary"><b>Criar Review</b></a>
</div>

<table class="table table-hover col-lg-12 tablesorter">
    <thead style="border: 1px solid silver; background-color: lightgrey">
        <tr>
            <th class="col-lg-2">Titulo</th>
            <th class="col-lg-1">Descrição</th>
            <th class="col-lg-1">Língua</th>
            <th class="col-lg-9" style="text-align: right">Opções</th>
        </tr>
    </thead>
    <tbody style="font-size:12px;">
        <?php foreach($reviews as $review):?>
        <tr>
            <td class="col-lg-1"><?php echo $review['title'] ?> </td>
            <td class="col-lg-6"><?php echo $review['description'] ?> </td>
            <td class="col-lg-1"><?php echo $review['name'] ?> </td>
            <td class="col-lg-4" style="text-align: right">
                <a type="button" href="<?php echo "/admin/modal/reviews/edit_reviews/".$review['idreviews'];?>" class="btn btn-primary" title="Editar review"> <span class="glyphicon glyphicon-cog"></span></a>
                <a type="button" href="<?php echo "/reviews/delete/".$review['idreviews'];?>" class="btn btn-primary" title="Eliminar review"> <span class="glyphicon glyphicon-remove"></span></a>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
