<div style="height:100%; width:100%;">
    <div class="col-lg-12" id="map-canvas"></div>
</div>
    
<style>
    #map-canvas {
        height: 90%;
        margin: 0px;
        margin-top: 50px;
        padding: 0px
    }
</style>

<script>
    var devices;
    var map_cars; 
    
    $(document).ready(function(){
       initialize(); 
    });
    
    function initialize(){
        map_cars = new GMaps({
            div: '#map-canvas',
            lat: 32.740316,
            lng: -16.943928,
            zoom: 11,
            zoomControl : true,
            enabled:false
        }); 
        
        $.ajax({
            url: "/admin/get_devices/",
            dataType: "json",
            success: function(data){
                devices = eval(data);
//                console.log(devices);
                
            },
            complete: function(){
                drawDevices(devices);
            },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
    }

    function drawDevices(points){
        points.forEach(function(point, key){
            if(point['latitude'] != null && point['longitude'] != null){
                var marker = map_cars.addMarker({
                    icon: 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                    lat: point['latitude'],
                    lng: point['longitude'],
                    title: point['iddevice'],
                    infoWindow: {
                        content: '<div style="width:200px"><p>Carro: '+point['iddevice']+'</p><p>Data: '+point['date']+'</p></div>'
                    }
                 }); 
            }
            if(key === points.length-1)
                updateCars();
        });
        
    }
    
    function updateCars(){
        setInterval(function(){
           $.ajax({
                url: "/admin/get_devices/",
                dataType: "json",
                success: function(data){
                    devices = eval(data);
                },
                complete: function(){
                    updatePosition(devices);
                },
                error: function (request, status, error) {
                    console.log("error - "+request.responseText);
                    console.log("error - "+status);
                }
            }); 
        },5000);
    }
    
    function updatePosition(devices){
        map_cars.markers.forEach(function(point){
            devices.forEach(function(point2){
                if(point.title == point2['iddevice']){
                    var coordinates = new google.maps.LatLng(point2['latitude'], point2['longitude']);
                    if(point.position.lat() != coordinates.lat() && point.position.lng() != coordinates.lng()){
                        setTimeout(function(){
                            point.setIcon('http://maps.google.com/mapfiles/ms/icons/blue-dot.png');
                        },500);
                        setTimeout(function(){
                            point.setIcon('http://maps.google.com/mapfiles/ms/icons/red-dot.png');
                        },2000);
                    
                        point.position = coordinates;
                        point.infoWindow.content = '<p>Carro: '+point2['iddevice']+'</p><p>Data: '+point2['date']+'</p>';
                    }
                }
            });
        });
    }

</script>
