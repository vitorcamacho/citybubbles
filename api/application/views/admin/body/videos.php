<form style="margin-top:70px; margin-left: 20px" enctype="multipart/form-data" action="<?php echo "/admin/insert_video"?>" method="POST">
    <div class="row">
        <div class="form-group col-lg-4">
          <label for="url">Youtube video ID</label>
          <input type="text" name="videoId" class="form-control" placeholder="Youtube Video ID">
        </div>
        <div class="col-xs-1">
            <label for="urlbtn">&nbsp;</label>
            <input type="submit" class="form-control btn btn-info"/>
        </div>
    </div>
</form>
<br><br>
<div class="col-lg-12">
  <table class="table table-hover col-lg-12 tablesorter">
      <thead style="border: 1px solid silver; background-color: lightgrey">
          <tr>
              <th class="col-lg-1">Preview</th>
              <th class="col-lg-10">URL</th>
              <th style="text-align: center" class="col-lg-1">Opções</th>
          </tr>
      </thead>
      <tbody style="font-size:12px;">
          <?php foreach($videos as $video):?>
          <tr>
              <td class="col-lg-1"><img src="<?php echo $video['preview']; ?>"</td>
              <td class="col-lg-10"><a href="<?php echo $video['videoId']; ?>"> <?php echo $video['videoId']; ?></a> </td>
              <td class="col-lg-1" style="text-align: center">
                  <a type="button" href="<?php echo "/admin/delete_video/".$video['id'];?>" class="btn btn-primary" title="Eliminar video"> <span class="glyphicon glyphicon-remove"></span></a>
              </td>
          </tr>
          <?php endforeach;?>
      </tbody>
  </table>
</div>
