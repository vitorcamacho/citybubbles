<div style="margin-bottom: 10px; margin-top: 70px; float:right" class="col-lg-2" >
    <a type="button" href="<?php echo "/admin/modal/slider/create_slider"?>" class="btn btn-primary"><b>Criar slider</b></a>
</div>

<table class="table table-hover col-lg-12 tablesorter">
    <thead style="border: 1px solid silver; background-color: lightgrey">
        <tr>
            <th class="col-lg-2">Imagem</th>
            <th class="col-lg-1">Língua</th>
            <th class="col-lg-9" style="text-align: right">Opções</th>
        </tr>
    </thead>
    <tbody style="font-size:12px;">
        <?php foreach($sliders as $slider):?>
        <tr>
            <td class="col-lg-2">
              <img style="margin-left: 10px" class="img-rounded"  width="200px" height="100px" src="<?php echo $slider['image'];?>">
            </td>
            <td class="col-lg-1"><?php echo $slider['name'] ?> </td>
            <td class="col-lg-9" style="text-align: right">
                <a type="button" href="<?php echo "/admin/modal/slider/edit_slider/".$slider['idslider'];?>" class="btn btn-primary" title="Editar slider"> <span class="glyphicon glyphicon-cog"></span></a>
                <a type="button" href="<?php echo "/slider/delete/".$slider['idslider'];?>" class="btn btn-primary" title="Eliminar slider"> <span class="glyphicon glyphicon-remove"></span></a>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
