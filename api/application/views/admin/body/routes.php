    <div style="margin-bottom: 10px; float:right;  margin-top: 70px; margin-right:20px" class="col-lg-1">
        <a type="button" href="<?php echo "/admin/create_route"?>" class="btn btn-primary"><b>Criar Rota</b></a>
    </div>

    <table class="table table-hover col-lg-12 tablesorter">
        <thead style="border: 1px solid silver; background-color: lightgrey">
            <tr>
                <th class="col-lg-1">Imagem</th>
                <th class="col-lg-1">Titulo</th>
                <th class="col-lg-7">Descrição</th>
                <th style="text-align: center" class="col-lg-3">Opções</th>
            </tr>
        </thead>
        <tbody style="font-size:12px;">
            <?php foreach($routes as $route):?>
            <tr>
                <?php if($route['image'] == "null") echo "<td class='col-lg-1'></td>";
                    else{?>
                <td><img style="margin-left: 10px" class="img-rounded"  width="50px" height="50px" src="<?php echo $route['image'];?>"></td>
                    <?php }?>
                <td class="col-lg-1"><?php echo $route['idroute']." - ".$route['info_name'] ?> </td>
                <td class="col-lg-7"><?php echo $route['description'] ?> </td>
                <td class="col-lg-3" style="text-align: center">
                    <a type="button" href="<?php echo "/admin/draw_route/".$route['idroute'];?>" class="btn btn-primary" title="Ver/Desenhar rota"> <span class="glyphicon glyphicon-pencil"></span></a>
                    <a type="button" href="<?php echo "/admin/load_route_modal/edit_route/".$route['idroute'];?>" class="btn btn-primary" title="Editar rota"> <span class="glyphicon glyphicon-cog"></span></a>
                    <a type="button" href="<?php echo "/admin/load_route_modal/translate_route/".$route['idroute'];?>" class="btn btn-primary" title="Traduções"> <span class="glyphicon glyphicon-globe"></span></a>
                    <a type="button" href="<?php echo "/admin/download/".$route['idroute']."/".$route['info_name'];?>" class="btn btn-primary" title="Download rota"> <span class="glyphicon glyphicon-download-alt"></span></a>
                    <a type="button" href="<?php echo "/admin/delete_route/".$route['idroute'];?>" class="btn btn-primary" title="Eliminar rota"> <span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>

<script>
$(document).ready(function(){
    $(".table").tablesorter();
});
</script>
