    <div style="margin-bottom: 10px; margin-top: 70px; float:right" class="col-lg-2" >
        <a type="button" href="<?php echo "/admin/create_pol"?>" class="btn btn-primary"><b>Criar Ponto de Localização</b></a>
    </div>

    <table class="table table-hover col-lg-12 tablesorter">
        <thead style="border: 1px solid silver; background-color: lightgrey">
            <tr>
                <th class="col-lg-1">Imagem</th>
                <th class="col-lg-3">Titulo</th>
                <th class="col-lg-6">Descrição</th>
                <th style="text-align: center" class="col-lg-2">Opções</th>
            </tr>
        </thead>
        <tbody style="font-size:12px;">
            <?php foreach($pols as $pol):?>
            <tr>
                <?php if($pol['pol_image'] == "null") echo "<td class='col-lg-1'></td>";
                    else{?>
                <td><img style="margin-left: 10px" class="img-rounded"  width="50px" height="50px" src="<?php echo $pol['pol_image'];?>"></td>
                    <?php }?>
                <td class="col-lg-3"><?php echo $pol['idpol']." - ".$pol['pol_name'] ?> </td>
                <td class="col-lg-6"><?php echo $pol['pol_description'] ?> </td>
                <td class="col-lg-2" style="text-align: center">
                    <a type="button" href="<?php echo "/admin/load_pol_modal/edit_pol/".$pol['idpol'];?>" class="btn btn-primary" title="Editar ponto de interesse"> <span class="glyphicon glyphicon-cog"></span></a>
                    <a type="button" href="<?php echo "/admin/load_pol_modal/translate_pol/".$pol['idpol'];?>" class="btn btn-primary" title="Traduções"> <span class="glyphicon glyphicon-globe"></span></a>
                    <a type="button" href="<?php echo "/admin/delete_pol/".$pol['idpol'];?>" class="btn btn-primary" title="Eliminar ponto de interesse"> <span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>


<script>
$(document).ready(function() { 
    $(".table").tablesorter(); 
}); 
</script>