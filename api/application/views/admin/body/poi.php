    <div style="margin-bottom: 10px; margin-top: 70px; float:right" class="col-lg-2" >
        <a type="button" href="<?php echo "/admin/create_poi"?>" class="btn btn-primary"><b>Criar Ponto de Interesse</b></a>
    </div>

    <table class="table table-hover col-lg-12 tablesorter">
        <thead style="border: 1px solid silver; background-color: lightgrey">
            <tr>
                <th class="col-lg-1">Imagem</th>
                <th class="col-lg-3">Titulo</th>
                <th class="col-lg-6">Descrição</th>
                <th style="text-align: center" class="col-lg-2">Opções</th>
            </tr>
        </thead>
        <tbody style="font-size:12px;">
            <?php foreach($pois as $poi):?>
            <tr>
                <?php if($poi['poi_image'] == "null") echo "<td class='col-lg-1'></td>";
                    else{?>
                <td><img style="margin-left: 10px" class="img-rounded"  width="50px" height="50px" src="<?php echo $poi['poi_image'];?>"></td>
                    <?php }?>
                <td class="col-lg-3"><?php echo $poi['idpoi']." - ".$poi['poi_name'] ?> </td>
                <td class="col-lg-6"><?php echo $poi['poi_description'] ?> </td>
                <td class="col-lg-2" style="text-align: center">
                    <a type="button" href="<?php echo "/admin/load_poi_modal/edit_poi/".$poi['idpoi'];?>" class="btn btn-primary" title="Editar ponto de interesse"> <span class="glyphicon glyphicon-cog"></span></a>
                    <a type="button" href="<?php echo "/admin/load_poi_modal/translate_poi/".$poi['idpoi'];?>" class="btn btn-primary" title="Traduções"> <span class="glyphicon glyphicon-globe"></span></a>
                    <a type="button" href="<?php echo "/admin/delete_poi/".$poi['idpoi'];?>" class="btn btn-primary" title="Eliminar ponto de interesse"> <span class="glyphicon glyphicon-remove"></span></a>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>


<script>
$(document).ready(function() { 
    $(".table").tablesorter(); 
}); 
</script>