<div style="margin-bottom: 10px; margin-top: 70px; float:right" class="col-lg-2" >
    <a type="button" href="<?php echo "/admin/modal/information/create_info"?>" class="btn btn-primary"><b>Criar Informação</b></a>
</div>

<table class="table table-hover col-lg-12 tablesorter">
    <thead style="border: 1px solid silver; background-color: lightgrey">
        <tr>
            <th class="col-lg-1">Imagem</th>
            <th class="col-lg-1">Língua</th>
            <th class="col-lg-2">Titulo</th>
            <th class="col-lg-6">Conteúdo</th>
            <th style="text-align: center" class="col-lg-2">Opções</th>
        </tr>
    </thead>
    <tbody style="font-size:12px;">
        <?php foreach($informations as $information):?>
        <tr>
            <td>
            <img style="margin-left: 10px" class="img-rounded"  width="50px" height="50px" src="<?php echo $information['image'];?>"></td>
            <td class="col-lg-1"><?php echo $information['name'] ?> </td>
            <td class="col-lg-2"><?php echo $information['title'] ?> </td>
            <td class="col-lg-6"><?php echo $information['content'] ?> </td>
            <td class="col-lg-2" style="text-align: center">
                <a type="button" href="<?php echo "/admin/modal/information/edit_info/".$information['idinformation'];?>" class="btn btn-primary" title="Editar informação"> <span class="glyphicon glyphicon-cog"></span></a>
                <a type="button" href="<?php echo "/information/delete/".$information['idinformation'];?>" class="btn btn-primary" title="Eliminar informação"> <span class="glyphicon glyphicon-remove"></span></a>
            </td>
        </tr>
        <?php endforeach;?>
    </tbody>
</table>
