<!-- edit pol -->
<div class="modal fade" id="edit_pol">
    <div class="modal-dialog" style="width: 95%; height: 400px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Editar Ponto de Localização</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12" id="map-canvas" style="width: 50%">
                    </div>   

                    <div class="col-lg-5">
                        <form enctype="multipart/form-data" action="<?php echo "/admin/edit_pol_submit"?>" method="POST" id="pol_form">
                            <div class="row">
                                <div class="col-xs-5">
                                    Latitude
                                    <input name="idpol_info" type="hidden" value="<?php echo $pol[0]['idpol_info'];?>"> 
                                    <input name="idpol" type="hidden" value="<?php echo $pol[0]['idpol'];?>"> 
                                    <input id="latitude" name="latitude" type="text" class="form-control" value="<?php echo $pol[0]['latitude'];?>" required> 
                                </div>
                                <div class="col-xs-5">
                                    Longitude
                                    <input id="longitude" name="longitude" type="text" class="form-control" value="<?php echo $pol[0]['longitude'];?>" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-11">
                                    Título
                                    <textarea type="text" name="name" class="form-control" maxlength="90" required><?php echo $pol[0]['pol_name'];?></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-11">
                                    Descrição
                                    <textarea style="height:80px" type="text" name="description" class="form-control" maxlength="1000" required><?php echo $pol[0]['pol_description'];?></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-11">
                                    Texto
                                    <textarea style="height:100px" type="text" name="text" class="form-control" maxlength="5000"><?php echo $pol[0]['pol_text'];?></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-2">
                                    <img class="img-rounded"  width="50px" height="50px" src="<?php echo $pol[0]['pol_image'];?>">
                                </div>
                                <div class="col-xs-9">
                                    <input type="file" class="form-control" name="image" id="image" />
                                </div>
                            </div>
                            <br>
                            <button type="submit" id="submit_button" data-loading-text="Aguarde..." class="btn btn-primary">Submeter </button>

                        </form>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</div>
        
<style>
    #map-canvas {
        height: 400px;
        width: 40%;
        margin-left: 10px;
        padding: 0px
    }
</style>


<script>
    $("#pol_form").validate();

    $('#edit_pol').on('hidden.bs.modal', function () {
        window.location = "/admin/pols";
    });
    
     $('#submit_button').click(function () {
        var btn = $(this)
        btn.button('loading');
    });

    $(document).ready(function(){
        var map;
        var marker = new google.maps.Marker({
             draggable: true,
             icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });

        $('#longitude').on('input', function() { 
            if($('#latitude').val() != ""){
                var marker_location = new google.maps.LatLng($('#latitude').val(),$('#longitude').val());
                marker.setPosition(marker_location);
            }
        });

        function initialize() {
            map = new GMaps({
                    div: '#map-canvas',
                    lat: 32.640316,
                    lng: -16.923928,
                    zoom: 13,
                    zoomControl : true,
                    click: function(event){
                        updateCoordinates(event, marker);
                    }
                });
                var marker_location = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
                marker.setPosition(marker_location);
                map.addMarker(marker);
        };

        google.maps.event.addListener(marker, 'drag', function(event) {
            updateCoordinates(event, marker);
        });        

        function updateCoordinates(evt, marker){
            var marker_location = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
            marker.setPosition(marker_location);
            $('#latitude').val(evt.latLng.lat());
            $('#longitude').val(evt.latLng.lng());
        }   

        $("#edit_pol").show(function () {
            initialize();
        });

});
</script>


