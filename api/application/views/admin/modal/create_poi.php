<div class="modal fade" id="create_poi">
    <div class="modal-dialog" style="width: 100%; height: 400px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Criar Ponto de Interesse</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12" id="map-canvas" style="width: 50%">
                    </div>   

                    <div class="col-lg-5">
                        <form enctype="multipart/form-data" action="<?php echo "/admin/create_poi_submit"?>" method="POST" id="poi_form">
                            <div class="row">
                                <div class="col-xs-5">
                                    Latitude
                                    <input id="latitude" name="latitude" type="text" class="form-control" placeholder="Clique no mapa" required> 
                                </div>
                                <div class="col-xs-5">
                                    Longitude
                                    <input id="longitude" name="longitude" type="text" class="form-control" placeholder="Clique no mapa" required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Título
                                    <textarea type="text" name="name" class="form-control" placeholder="Texto em portugês" maxlength="90" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Descrição
                                    <textarea style="height:50px" type="text" name="description" class="form-control" placeholder="Texto em portugês" maxlength="1000" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Texto
                                    <textarea style="height:100px" type="text" name="text" class="form-control" placeholder="Texto em portugês" maxlength="5000"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Imagem
                                    <input type="file" class="form-control" name="image" id="image" />
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Submeter</button>
                        </form>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</div>
        
<style>
    #map-canvas {
        height: 400px;
        width: 40%;
        margin-left: 10px;
        padding: 0px
    }
</style>
    
<script>
    $(document).ready(function(){
        var map;
        var pois = new Array();
        var marker = new google.maps.Marker({
             draggable: true,
             icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
       
        $('#longitude').on('input', function() { 
            if($('#latitude').val() != ""){
                var marker_location = new google.maps.LatLng($('#latitude').val(),$('#longitude').val());
                marker.setPosition(marker_location);
            }
        });
       
        function initialize() {
            map = new GMaps({
                    div: '#map-canvas',
                    lat: 32.640316,
                    lng: -16.923928,
                    zoom: 13,
                    zoomControl : true,
                    click: function(event){
                        updateCoordinates(event, marker);
                        map.addMarker(marker);
                    }
                });            
        };
        
        google.maps.event.addListener(marker, 'drag', function(event) {
            updateCoordinates(event, marker);
        });        

        function updateCoordinates(evt, marker){
            var marker_location = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
            marker.setPosition(marker_location);
            $('#latitude').val(evt.latLng.lat());
            $('#longitude').val(evt.latLng.lng());
        }   
        
        $("#create_poi").show(function () {
            initialize();
            loadPoi(map);
        });
        
        $("#poi_form").validate();
        
        $('#create_poi').on('hidden.bs.modal', function () {
            window.location = "/admin/";
        });
    });
    
    function loadPoi(map_obj){
         $.ajax({
            url: "/admin/get_poi", 
            cache:false,
            contentType: false,
            processData: false,
            success: 
                function(data){
                    pois = eval(data);
                },
            complete:function(){
                drawPoi(map_obj, pois);
            },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
    }
    
    function drawPoi(map_obj, points){
        var myIcon = new google.maps.MarkerImage('/application/assets/img/map/poi_black.png', null, null, null, new google.maps.Size(50,50));

        points.forEach(function(point){
            map_obj.addMarker({
                lat: point['latitude'],
                lng: point['longitude'],
                title: point['poi_name'],
                icon: myIcon,
                infoWindow: {
                      content: '<div style="text-align:center"><img class="img-rounded" src="'+point['poi_image']+'" width="50" height="50"> <br><br><b>'+point['poi_name']+'</b></div>'
                }
            }); 
        });
    }
    
    </script>