<!-- translate poi -->
<div class="modal fade " id="translate_gpoint" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 70%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"> Traduções do Ponto de Carga</h4>
            </div>
            <div class="modal-body">
            <form enctype="multipart/form-data" action="javascript:post();" method="POST" id="gpoint_form">
                <div class="row">    
                    <div class="col-xs-3">
<!--                        Língua
-->                        <select id="select_language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>
                
                <?php foreach ($gpoints_info as $element): ?>
               
                <div style="display: none;" class="row col-xs-12" id="<?php echo $element['name'];?>">
                    <div class="row">    
                        <div class="col-xs-12">
                                <input name="idgreen_point_info" type="hidden" value="<?php echo $element['idgreen_point_info'];?>"> 
                                <input name="idlanguage" type="hidden" value="<?php echo $element['idgreen_point_info'];?>"> 
                             Texto
                            <textarea style="height:120px" type="text" name="text1" class="form-control" maxlength="180"><?php echo $element['text'];?></textarea>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";

                $("#gpoint_form").validate();


                $('#translate_gpoint').on('hidden.bs.modal', function () {
                    window.location = "/admin/gpoints";
                });

                $(document).ready(function(){
                    $('#'+active).show();
                });

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });
            
                function post(a){
                    var num = $('[name="idgreen_point_info"]').length-1;
                    postData(num);
                }
                
                function postData(a){
                    if(a < 0){                        
                        window.location = "/admin/gpoints";
                    }
                    else
                        var num = a;
                    formData = {
                        idgreen_point_info:$('[name="idgreen_point_info"]').eq(num).val(),
                        text: $('[name="text1"]').eq(num).val(),
                        idlanguage: $('[name="idlanguage"]').eq(num).val()
                    };
            
                    $.ajax({
                        type: "POST",
                        url: "/admin/update_gpoint_info/",
                        data: formData,
                        success: function(data){
                            console.log(data);
                        },
                        complete: function(data){
                            console.log(data);
                            num = num - 1;
                            postData(num);
                        },
                        error: function (request, status, error) {
                            console.log("error - "+request.responseText);
                            console.log("error - "+status);
                        }
                    });
                }
            
            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
