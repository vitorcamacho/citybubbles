<!-- translate poi -->
<div class="modal fade " id="edit_info" enctype="multipart/form-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar informação</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/information/edit_info_submit"?>" enctype="multipart/form-data" method="POST" id="create_info_form">
                <input name="language" type="hidden" value="<?php echo $information['idlanguage'];?>" />
                <input name="idinformation" type="hidden" value="<?php echo $information['idinformation'];?>" />
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Imagem
                            <input type="file" class="form-control" id="info_image" name="image" id="image" />
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Título
                            <input type="text" name="title" class="form-control" value="<?php echo $information['title'];?>"></input>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Conteúdo
                            <textarea style="height:120px" type="text" name="content" class="form-control" maxlength="5000"><?php echo $information['content'];?></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_info_form").validate();

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });

            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
