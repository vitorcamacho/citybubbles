<!-- translate poi -->
<div class="modal fade " id="edit_reviews" enctype="multipart/form-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar review</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/reviews/edit_reviews_submit"?>" enctype="multipart/form-data" method="POST" id="create_reviews_form">
                <input name="language" type="hidden" value="<?php echo $review['idlanguage'];?>" />
                <input name="idreviews" type="hidden" value="<?php echo $review['idreviews'];?>" />
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Título
                            <input type="text" class="form-control" id="title" name="title" id="title" value="<?php echo $review['title']?>" />
                        </div>
                    </div>
                    <br>
                </div>
                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Descrição
                            <textarea type="text" class="form-control" id="description" name="description" id="description"><?php echo $review['description']?></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_review_form").validate();

                function readFile() {
                  if (this.files && this.files[0]) {
                    var FR= new FileReader();
                    FR.onload = function(e) {
                      image = e.target.result;
                    };
                    var b64 = FR.readAsDataURL( this.files[0] );

                  }
                }
                document.getElementById("info_image").addEventListener("change", readFile, false);

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });

                function post(){
                    var formData = {
                        image:image,
                        idreviews:$('[name="idreviews"]').val(),
                        language:$('[name="language"]').val(),
                    };

                    $.post("/reviews/edit_reviews_submit", formData)
                      .done(function(data){
                        window.location = "/admin/reviews";
                      })
                }

            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
