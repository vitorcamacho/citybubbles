<!-- translate poi -->
<div class="modal fade " id="edit_slider" enctype="multipart/form-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar slider</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/slider/edit_slider_submit"?>" enctype="multipart/form-data" method="POST" id="create_slider_form">
                <input name="language" type="hidden" value="<?php echo $slider['idlanguage'];?>" />
                <input name="idslider" type="hidden" value="<?php echo $slider['idslider'];?>" />
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Imagem
                            <input type="file" class="form-control" id="info_image" name="image" id="image" />
                        </div>
                    </div>
                    <br>
                </div>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_slider_form").validate();

                function readFile() {
                  if (this.files && this.files[0]) {
                    var FR= new FileReader();
                    FR.onload = function(e) {
                      image = e.target.result;
                    };
                    var b64 = FR.readAsDataURL( this.files[0] );

                  }
                }
                document.getElementById("info_image").addEventListener("change", readFile, false);

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });

                function post(){
                    var formData = {
                        image:image,
                        idslider:$('[name="idslider"]').val(),
                        language:$('[name="language"]').val(),
                    };

                    $.post("/slider/edit_slider_submit", formData)
                      .done(function(data){
                        window.location = "/admin/slider";
                      })
                }

            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
