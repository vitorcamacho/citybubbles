<div class="modal fade" id="edit_gpoint">
    <div class="modal-dialog" style="width: 95%; height: 500px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Editar Ponto de Carga</h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12" id="map-canvas" style="width: 100%"></div>
                <input id="idgreen_point" type="hidden" value="<?php echo $gpoint[0]['idgreen_point'];?>"> 
                <input id="latitude" type="hidden" name="latitude" type="text" class="form-control" value="<?php echo $gpoint[0]['latitude'];?>" required> 
                <input id="longitude" type="hidden" name="longitude" type="text" class="form-control" value="<?php echo $gpoint[0]['longitude'];?>" required>
                <a style="margin-top:10px; width:100%;" type="button" href="javascript:post_gpoint();" class="btn btn-primary btn-lg" title="Guardar">Guardar</a>
            </div>    
        </div>
    </div>
</div>
        
<style>
    #map-canvas {
        height: 400px;
        width: 40%;
        margin-left: 10px;
        padding: 0px
    }
</style>


<script>
    $('#edit_gpoint').on('hidden.bs.modal', function () {
        window.location = "/admin/gpoints";
    });
    
     $('#submit_button').click(function () {
        var btn = $(this)
        btn.button('loading');
    });

    $(document).ready(function(){
        var map;
        var marker = new google.maps.Marker({
             draggable: true,
             icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
        
        function initialize() {
            map = new GMaps({
                    div: '#map-canvas',
                    lat: 32.640316,
                    lng: -16.923928,
                    zoom: 13,
                    zoomControl : true,
                    click: function(event){
                        updateCoordinates(event, marker);
                    }
                });
                var marker_location = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
                marker.setPosition(marker_location);
                map.addMarker(marker);
        };

        google.maps.event.addListener(marker, 'drag', function(event) {
            updateCoordinates(event, marker);
        });        

        function updateCoordinates(evt, marker){
            var marker_location = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
            marker.setPosition(marker_location);
            $('#latitude').val(evt.latLng.lat());
            $('#longitude').val(evt.latLng.lng());
        }   

        $("#edit_gpoint").show(function () {
            initialize();
        });

    });
    
    function post_gpoint(){
        $.ajax({
            type: "POST",
            url: "/admin/edit_gpoint_submit/",
            data: {
                idgreen_point: $('#idgreen_point').val(),
                latitude: $('#latitude').val(),
                longitude: $('#longitude').val()
            },
            success: function(e){
//                console.log(e);
            },
            complete: function(e){
                 window.location = "/admin/gpoints";
            },
            error: function (request, status, error) {
            //                console.log("error - "+request.responseText);
            //                console.log("error - "+status);
            }
        });
    }
</script>


