<!-- translate pol -->
<div class="modal fade " id="translate_pol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Traduções do ponto de interesse</h4>
            </div>
            <div class="modal-body">
            <form enctype="multipart/form-data" action="javascript:post();" method="POST" id="pol_form">
                <div class="row">    
                    <div class="col-xs-3">
                        Língua
                        <select id="select_language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>
                
                <?php foreach ($pol as $element): ?>
               
                <div style="display: none;" class="row col-xs-12" id="<?php echo $element['language_name'];?>">
                    <div class="row">    
                        <div class="col-xs-12">
                                <input name="idpol_info" type="hidden" value="<?php echo $element['idpol_info'];?>"> 
                                <input name="idpol" type="hidden" value="<?php echo $element['idpol'];?>"> 
                                <input name="idlanguage" type="hidden" value="<?php echo $element['idlanguage'];?>"> 
                            Título
                            <textarea type="text" name="name1" class="form-control" maxlength="90" ><?php echo $element['pol_name'];?></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Descrição
                            <textarea style="height:80px" type="text" name="description1" class="form-control" maxlength="1000" ><?php echo $element['pol_description'];?></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Texto
                            <textarea style="height:120px" type="text" name="text1" class="form-control" maxlength="5000"><?php echo $element['pol_text'];?></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <?php endforeach;?>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";

                $("#pol_form").validate();


                $('#translate_pol').on('hidden.bs.modal', function () {
                    window.location = "/admin/pols";
                });

                $(document).ready(function(){
                    $('#'+active).show();
                });

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });
            
                function post(a){
                    var num = $('[name="idpol_info"]').length-1;
                    postData(num);
                }
                
                function postData(a){
                    if(a < 0){                        
                        window.location = "/admin/pols";
                    }
                    else
                        var num = a;
                    formData = {
                        idpol_info:$('[name="idpol_info"]').eq(num).val(),
                        name:$('[name="name1"]').eq(num).val(),
                        description:$('[name="description1"]').eq(num).val(),
                        text: $('[name="text1"]').eq(num).val(),
                        idpol: $('[name="idpol"]').eq(num).val()
                    };
            
                    $.ajax({
                        type: "POST",
                        url: "/admin/update_pol_info/",
                        data: formData,
                        success: function(data){
                            console.log(data);
                        },
                        complete: function(data){
                            console.log(data);
                            num = num - 1;
                            postData(num);
                        },
                        error: function (request, status, error) {
                            console.log("error - "+request.responseText);
                            console.log("error - "+status);
                        }
                    });
                }
            
            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate pol -->
