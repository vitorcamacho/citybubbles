<!-- edit route -->
<div class="modal fade " id="edit_route" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Editar Rota</h4>
            </div>
            <div class="modal-body">
            <form enctype="multipart/form-data" action="<?php echo "/admin/edit_route_submit"?>" method="POST" id="route_form">
                <div class="row">
                    <input name="idroute_info" type="hidden" value="<?php echo $route[0]['idroute_info'];?>"> 
                    <input name="idroute" type="hidden" value="<?php echo $route[0]['idroute'];?>"> 
                    <div class="col-xs-6">
                        <div class="col-xs-3">
                            <img class="img-rounded"  width="100px" height="100px" src="<?php echo $route[0]['image'];?>">
                        </div>
                        <div class="col-xs-8">
                            <input type="file" class="form-control" name="image" id="image" />
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">    
                    <div class="col-xs-6">
                        Título
                        <textarea type="text" name="name" class="form-control" maxlength="90" required><?php echo $route[0]['info_name'];?></textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-xs-12">
                        Descrição
                        <textarea style="height:80px" type="text" name="description" class="form-control" maxlength="5000" required><?php echo $route[0]['description'];?></textarea>
                    </div>
                </div>
                <br>
                <br>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
            $("#route_form").validate();
            
            
            $('#edit_route').on('hidden.bs.modal', function () {
                window.location = "/admin/routes";
            });
            
            
            </script>
            </div>
        </div>
    </div>
</div>


