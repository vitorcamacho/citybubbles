<!-- translate poi -->
<div class="modal fade" id="create_slider" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Criar slider</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/slider/create_slider_submit"?>" enctype="multipart/form-data" method="POST" id="create_slider_form">
                <div class="row">
                    <div class="col-xs-3">
                        Língua
                        <select id="select_language" name="language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option name="language" value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Imagem
                            <input type="file" class="form-control" id="info_image" name="image" id="image" />
                        </div>
                    </div>
                    <br>
                </div>
                <input type="submit" name="submit" value="Submit" />
                <!-- <button type="submit" class="btn btn-primary">Submeter</button> -->
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_slider_form").validate();

                // function readFile() {
                //   if (this.files && this.files[0]) {
                //     var FR= new FileReader();
                //     FR.onload = function(e) {
                //       image = e.target.result;
                //     };
                //     var b64 = FR.readAsDataURL( this.files[0] );
                //
                //   }
                // }
                // document.getElementById("info_image").addEventListener("change", readFile, false);

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });

                // function post(){
                //     var formData = {
                //         image:image,
                //         language:$('[name="language"]').val(),
                //     };
                //
                //     $.post("/slider/create_slider_submit", formData)
                //       .done(function(data){
                //         window.location = "/admin/slider";
                //       });
                // }

            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
