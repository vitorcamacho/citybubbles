<!-- translate route -->
<div class="modal fade " id="translate_route" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Traduções da Rota</h4>
            </div>
            <div class="modal-body">
            <form enctype="multipart/form-data" action="javascript:post();" method="POST" id="route _form">
                <div class="row">    
                    <div class="col-xs-3">
                        Língua
                        <select id="select_language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>
                
                <?php foreach ($route as $element): ?>
               
                <div style="display: none;" class="row col-xs-12" id="<?php echo $element['language_name'];?>">
                    <div class="row">    
                        <div class="col-xs-12">
                                <input name="idroute_info2" type="hidden" value="<?php echo $element['idroute_info'];?>"> 
                                <input name="idroute2" type="hidden" value="<?php echo $element['idroute'];?>"> 
                                <input name="idlanguage2" type="hidden" value="<?php echo $element['idlanguage'];?>"> 
                            Título
                            <textarea type="text" name="name2" class="form-control" maxlength="90" ><?php echo $element['info_name'];?></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Descrição
                            <textarea style="height:80px" type="text" name="description2" class="form-control" maxlength="5000" ><?php echo $element['description'];?></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <?php endforeach;?>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";

                $("#route_form").validate();


                $('#translate_route').on('hidden.bs.modal', function () {
                    window.location = "/admin/routes";
                });

                $(document).ready(function(){
                    $('#'+active).show();
                });

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });
            
                function post(a){
                    var num = $('[name="idroute_info2"]').length-1;
                    postData(num);
                }
                
                function postData(a){
                    console.log(a);
                    if(a < 0)
                        window.location = "/admin/routes";
                    else
                        var num = a;
                    
                    
                    formData = {
                        idroute_info:$('[name="idroute_info2"]').eq(num).val(),
                        name:$('[name="name2"]').eq(num).val(),
                        description:$('[name="description2"]').eq(num).val(),
                        idroute: $('[name="idroute2"]').eq(num).val()
                    };
                                
                    $.ajax({
                        type: "POST",
                        url: "/admin/update_route_info/",
                        data: formData,
                        success: function(data){
                            console.log(data);
                        },
                        complete: function(data){
                            console.log(data);
                            num = num - 1;
                            postData(num);
                        },
                        error: function (request, status, error) {
                            console.log("error - "+request.responseText);
                            console.log("error - "+status);
                        }
                    });
                }
            
            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate route -->
