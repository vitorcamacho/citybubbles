<div class="modal fade" id="create_gpoint">
    <div class="modal-dialog" style="width: 100%; height: 400px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Criar Ponto de Carga</h4>
            </div>
            <div class="modal-body">
                    <input type="hidden" id="latitude" name="latitude" type="text"> 
                    <input type="hidden" id="longitude" name="longitude" type="text">
                
                    <div class="col-lg-12" id="map-canvas" style="width: 100%"></div>
                    <a style="margin-top:10px; width:100%;" type="button" href="javascript:post();" class="btn btn-primary btn-lg" title="Guardar">Guardar</a>
            </div>
        </div>
    </div>
</div>
        
<style>
    #map-canvas {
        height: 500px;
        width: 100%;
        margin-left: 10px;
        padding: 0px
    }
</style>
    
<script>
    $(document).ready(function(){
        var map;
        var gpoints = new Array();
        var marker = new google.maps.Marker({
             draggable: true,
             icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
        });
       
        function initialize() {
            map = new GMaps({
                    div: '#map-canvas',
                    lat: 32.640316,
                    lng: -16.923928,
                    zoom: 13,
                    zoomControl : true,
                    click: function(event){
                        updateCoordinates(event, marker);
                        map.addMarker(marker);
                    }
                });            
        };
        
        google.maps.event.addListener(marker, 'drag', function(event) {
            updateCoordinates(event, marker);
        });        

        function updateCoordinates(evt, marker){
            var marker_location = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
            marker.setPosition(marker_location);
            $('#latitude').val(evt.latLng.lat());
            $('#longitude').val(evt.latLng.lng());
        }   
        
        $("#create_gpoint").show(function () {
            initialize();
            loadGpoint(map);
        });
                
        $('#create_gpoint').on('hidden.bs.modal', function () {
            window.location = "/admin/gpoints";
        });

    });
    
    function loadGpoint(map_obj){
         $.ajax({
            url: "/admin/get_gpoints", 
            cache:false,
            contentType: false,
            processData: false,
            success: 
                function(data){
                    gpoints = eval(data);
                },
            complete:function(){
                drawGpoint(map_obj, gpoints);
            },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
    }
    
    function drawGpoint(map_obj, points){
        var myIcon = new google.maps.MarkerImage('/application/assets/img/map/gpoint.png', null, null, null, new google.maps.Size(50,50));

        points.forEach(function(point){
            console.log(point['idgreen_point'],point['latitude'], point['longitude']);
            map_obj.addMarker({
                lat: point['latitude'],
                lng: point['longitude'],
                icon: myIcon,
                click: function(marker){
                    console.log(marker.position);
                }
            }); 
        });
    }
    
    function post(){
        $.ajax({
            type: "POST",
            url: "/admin/create_gpoint_submit/",
            data: {
              latitude: $('#latitude').val(),
              longitude: $('#longitude').val()
            },
            success: function(e){
                console.log(e);
            },
            complete: function(e){
                 window.location = "/admin/gpoints";
            },
            error: function (request, status, error) {
//                console.log("error - "+request.responseText);
//                console.log("error - "+status);
            }
        });
    }
    
    </script>