<!-- translate poi -->
<div class="modal fade " id="create_info" enctype="multipart/form-data" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Criar Informação</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/information/create_info_submit"?>" enctype="multipart/form-data" method="POST" id="create_info_form">
                <div class="row">
                    <div class="col-xs-3">
                        Língua
                        <select id="select_language" name="language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option name="language" value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Imagem
                            <input type="file" class="form-control" id="info_image" name="image" id="image" />
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Título
                            <input type="text" name="title" class="form-control" ></input>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            Conteúdo
                            <textarea style="height:120px" type="text" name="content" class="form-control" maxlength="5000"></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <button type="submit" class="btn btn-primary">Submeter</button>
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_info_form").validate();

                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });
            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
