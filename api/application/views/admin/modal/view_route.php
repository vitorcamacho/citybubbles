<div class="modal fade" id="create_poi">
    <div class="modal-dialog" style="width: 100%; height: 400px;">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                 <h4 class="modal-title">Criar Ponto de Interesse</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6" id="map-canvas">
                    </div>   

                    <div class="col-lg-5 col-lg-offset-1">
                        <form enctype="multipart/form-data" action="<?php echo "/admin/create_poi_submit"?>" method="POST" id="poi_form">
                            <div class="row">
                                <div class="col-xs-5">
                                    Latitude
                                    <input id="latitude" name="latitude" type="text" class="form-control" placeholder="Clique no mapa" readonly required> 
                                </div>
                                <div class="col-xs-5">
                                    Longitude
                                    <input id="longitude" name="longitude" type="text" class="form-control" placeholder="Clique no mapa" readonly required>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Título
                                    <textarea type="text" name="name" class="form-control" placeholder="Texto em portugês" maxlength="90" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Descrição
                                    <textarea style="height:50px" type="text" name="description" class="form-control" placeholder="Texto em portugês" maxlength="1000" required></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    Texto
                                    <textarea style="height:100px" type="text" name="text" class="form-control" placeholder="Texto em portugês (opcional)" maxlength="5000"></textarea>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-10">
                                    <input type="file" class="form-control" name="image" id="image" />
                                </div>
                            </div>
                            <br>
                            <button type="submit" class="btn btn-primary">Submeter</button>
                        </form>
                    </div>
                </div>    
            </div>    
        </div>
    </div>
</div>
        
   <style>
    #map-canvas {
        height: 400px;
        width: 40%;
        margin-left: 10px;
        padding: 0px
    }
</style>
    
    <script>
    $(document).ready(function(){
       var map;
       var marker = new google.maps.Marker({
            draggable: true,
            icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png'
       });
    
        function initialize() {
            map = new GMaps({
                    div: '#map-canvas',
                    lat: 32.640316,
                    lng: -16.923928,
                    zoom: 13,
                    zoomControl : true,
                    click: function(event){
                        updateCoordinates(event, marker);
                        map.addMarker(marker);
                    }
                });
        };
        
        google.maps.event.addListener(marker, 'drag', function(event) {
            updateCoordinates(event, marker);
        });        

        function updateCoordinates(evt, marker){
            var marker_location = new google.maps.LatLng(evt.latLng.lat(),evt.latLng.lng());
            marker.setPosition(marker_location);
            $('#latitude').val(evt.latLng.lat());
            $('#longitude').val(evt.latLng.lng());
        }   
        
        $("#create_poi").show(function () {
            initialize();
        });
        
        $("#poi_form").validate();
        
        $('#create_poi').on('hidden.bs.modal', function () {
            window.location = "/admin/";
        });
        
    });
    
    </script>
    
    <script>
        $.ajax({
            url: "/admin/get_poi", 
            cache:false,
            contentType: false,
            processData: false,
            success: 
                function(data){
                    data = eval(data);
                    for(var i=0; i < data.length; i++){
                        var marker_location = new google.maps.LatLng(data[i]['latitude'],data[i]['longitude']);
                        new google.maps.Marker({
                            position: marker_location,
                            map: map,
                            title: data[i]['info_name']
                        });
                    }
                    
                },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
   
</script>