<!-- login -->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Admin Login</h4>
            </div>
            <div class="modal-body">
                <form action="<?php echo "/admin/login"?>" method="POST">
                    <div class="row">
                        <div class="col-xs-2  col-lg-offset-1">
                             Username
                        </div>
                        <div class="col-xs-6">
                            <input type="text" class="form-control" placeholder="" name="username">
                        </div>
                    </div>
                    &nbsp;
                    <div class="row">
                        <div class="col-xs-2  col-lg-offset-1">
                             Password
                        </div>
                        <div class="col-xs-6">
                            <input type="password" class="form-control" placeholder="" name="password">
                        </div>
                    </div>
                    <br>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /login -->

<script>

$('#login').on('hidden.bs.modal', function () {
    window.location = "/admin";
});


</script>