<!-- translate poi -->
<div class="modal fade" id="create_reviews" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 80%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Criar review</h4>
            </div>
            <div class="modal-body">
            <form action="<?php echo "/reviews/create_reviews_submit"?>" enctype="multipart/form-data" method="POST" id="create_review_form">
                <div class="row">
                    <div class="col-xs-3">
                        Língua
                        <select id="select_language" name="language" class="form-control">
                            <?php foreach ($languages as $language): ?>
                            <option name="language" value="<?php echo $language['idlanguage'];?>"><?php echo $language['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <br>

                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Título
                            <input type="text" class="form-control" id="title" name="title" id="title" />
                        </div>
                    </div>
                    <br>
                </div>
                <div class="row col-xs-12">
                    <div class="row">
                        <div class="col-xs-10">
                            Descrição
                            <textarea type="text" class="form-control" id="description" name="description" id="description"></textarea>
                        </div>
                    </div>
                    <br>
                </div>
                <input type="submit" name="submit" value="Submit" class="btn btn-primary" />
            </form>
            <script>
                var active = "PT";
                var image;

                $("#create_review_form").validate();
                $('#select_language').on('change',function(){
                    var language = $("option:selected").text();
                    $('#'+active).hide();
                    active = language;
                    $('#'+active).show();
                });

            </script>
            </div>
        </div>
    </div>
</div>
<!-- translate poi -->
