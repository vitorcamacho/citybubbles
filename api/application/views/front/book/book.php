<!-- Booking -->
<div class="modal fade" id="booking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $rbundle->bookingContent->title; ?></h4>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" action="<?php echo "/home/booking" ?>" method="POST" id="enbooking_form">
                    <div class="row">
                        <div class="col-xs-6">
                            <?php echo $rbundle->bookingContent->first_name; ?>
                            <input type="text" name="first_name" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-xs-6">
                          <?php echo $rbundle->bookingContent->last_name; ?>
                            <input type="text" name="last_name" class="form-control" placeholder="" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-2">
                            <?php echo $rbundle->bookingContent->age; ?>
                            <input type="text" name="age" class="form-control" required>
                        </div>
                        <div class="col-xs-4">
                            <?php echo $rbundle->bookingContent->contact; ?>
                            <input type="text" name="contact" class="form-control" required>
                        </div>
                        <div class="col-xs-6">
                            <?php echo $rbundle->bookingContent->email; ?>
                            <input type="email" name="email" class="form-control" required>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-12">
                            <label class="radio-inline">
                                <input type="radio" name="arrival_type" value="Hotel" required>
                                <strong><?php echo $rbundle->bookingContent->hotel; ?></strong>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="arrival_type" value="Cruzeiro" required>
                                <strong><?php echo $rbundle->bookingContent->cruise; ?></strong>
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="arrival_type" value="Residente" required>
                                <strong><?php echo $rbundle->bookingContent->resident; ?></strong>
                            </label>
                        </div>
                        <div class="col-xs-8" style="margin-top:10px">
                            <input type="text" name="arrival_type_desc" placeholder="" class="form-control" required>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12" style="margin-bottom: 20px">
                            <div class="col-xs-4 no-padding">
                                <?php echo $rbundle->bookingContent->begin_date; ?>
                                <input type="date" name="begin_date" class="form-control text-center" required>
                            </div>
                            <div class="col-xs-3">
                                <?php echo $rbundle->bookingContent->begin_time; ?>
                                <input class="form-control text-center" name="begin" type="time" required>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-4 no-padding">
                                <?php echo $rbundle->bookingContent->end_date; ?>
                                <input type="date" name="end_date" class="form-control text-center" required>
                            </div>
                            <div class="col-xs-3">
                                <?php echo $rbundle->bookingContent->end_time; ?>
                                <input class="form-control text-center" name="end" type="time" required>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-4">
                            <?php echo $rbundle->bookingContent->num_cars; ?>
                            <input class="form-control" name="num_pass" value="1" min="1" max="10" type="number" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="cancel" type="button" class="btn btn-default" data-dismiss="modal">
                      <?php echo $rbundle->bookingContent->cancel; ?>
                    </button>
                    <button type="submit" class="btn btn-primary">
                      <?php echo $rbundle->bookingContent->submit; ?>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- /Booking -->
