<!-- Booking -->
        <div class="modal fade" id="conditions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="myModalLabel"><?php echo $rbundle->pricesContent->modal->title; ?></h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-sm-8">
                                <ul>
                                  <?php foreach ($rbundle->pricesContent->modal->conditions as $condition): ?>
                                    <li><?php echo $condition?></li>
                                    &nbsp;
                                  <?php endforeach; ?>
                                </ul>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button id="cancel" type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /Booking -->
