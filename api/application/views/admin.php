<!DOCTYPE html>
<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" href="<?php echo "/application/assets/img/car.png"?>">

        <title>City Bubbles - Admin</title>

        <!-- Bootstrap Core CSS -->
        <link href="<?php echo "/application/assets/"?>css/bootstrap.css" rel="stylesheet">
        <link href="<?php echo "/application/assets/"?>css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css"/>

        <!-- jQuery Version 1.11.0 -->
        <script type="text/javascript" src="<?php echo "/application/assets/"?>js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script type="text/javascript" src="<?php echo "/application/assets/"?>js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo "/application/assets/"?>js/jquery.validate.min.js"></script>

        <!-- Custom-->
        <script type="text/javascript" src="<?php echo "/application/assets/"?>js/jquery.tablesorter.js"></script>

         <!-- Google Map-->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg5buuARNQozJLurycMyv8M4QukfnMvks&sensor=TRUE">></script>
        <script type="text/javascript" src="<?php echo "/application/assets/"?>js/gmaps.js"></script>

        <style>
            html, body {
              height: 100%;
              margin: 0px;
              padding: 0px
            }
        </style>
    </head>

    <body>
        <?php
        if($this->session->userdata('username') == null){
            $this->load->view("admin/modal/login");
            echo "<script type='text/javascript'>
                    $(document).ready(function(){
                        $('#login').modal('show');
                    });
                  </script>";
        }
        else{
            $this->load->view("admin/header/header");
            if(!isset($url) || $url == null)
                $this->load->view("admin/body/poi");
            else{
                $this->load->view("admin/body/".$url);
            }

            // for modals
            if(isset($modal) && $modal == "true"){
                if(isset($poi))
                    $data['poi'] = $poi;
                else if(isset($route))
                    $data['route'] = $route;
                else if(isset($pols))
                    $data['pols'] = $pols;
                else if(isset($gpoints))
                    $data['gpoints'] = $gpoints;
                else if(isset($information))
                    $data['information'] = $information;
                  echo "admin/modal/".$modal_page;
                $data['$languages'] = $languages;
                $this->load->view("admin/modal/".$modal_page, $data);

                echo "<script type='text/javascript'>
                    $(document).ready(function(){
                        $('#$modal_page').modal('show');
                    });
                  </script>";
            }
            // /for modals
        }
        ?>

    </body>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-54228196-1', 'auto');
        ga('send', 'pageview');
    </script>

</html>
