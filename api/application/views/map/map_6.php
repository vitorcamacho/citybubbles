<script type="text/javascript" src="<?php echo "/application/assets/"?>js/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg5buuARNQozJLurycMyv8M4QukfnMvks&sensor=TRUE&language=fr"></script>
<script type="text/javascript" src="<?php echo "/application/assets/"?>js/gmaps.js"></script>
Traduzindo Frances
<table style="border: 1px solid grey">
    <tr>
        <td>1</td>
        <td>PT</td>
        <td>[X]</td>
    </tr>
    <tr>
        <td>2 -</td>
        <td>EN</td>
        <td>[X]</td>
    </tr>
    <tr>
        <td>3 -</td>
        <td>ES</td>
        <td>[X]</td>
    </tr>
    <tr>
        <td>4 -</td>
        <td>DE</td>
        <td>[X]</td>
    </tr>
    <tr>
        <td>5 -</td>
        <td>FI</td>
        <td>[X]</td>
    </tr>
    <tr>
        <td>6 -</td>
        <td>FR</td>
        <td>[-]</td>
    </tr>
    <tr>
        <td>7 -</td>
        <td>IT</td>
        <td>[-]</td>
    </tr>
    <tr>
        <td>8 -</td>
        <td>RU</td>
        <td>[-]</td>
    </tr>
</table>

<div style="display: none" id="map-canvas"></div>

<script>
var idroute, route_points, all_routes;

$(document).ready(function(){
    map = new GMaps({
        div: '#map-canvas',
        lat: 32.640316,
        lng: -16.923928,
        zoom: 14,
        zoomControl : true,
        enabled:false,
    });  
    initialize();
    
});



function initialize(){
    var url = document.URL;
    var id = url.split("/");
    
    idroute = id[id.length-2];
    $.ajax({
        url: "/admin/get_route_lat/"+idroute,
        dataType: "json",
        success: function(data){
            var info = eval(data);
            route_points = info;
        },
        complete: function(){
            getRoutes(route_points);
        },
        error: function (request, status, error) {
            console.log("error - "+request.responseText);
            console.log("error - "+status);
        }
    });
}

function getRoutes(points){
        all_routes = new Array();
        var index = 0;
        var length = points.length-1;
       
        if(length >= 0)
            drawLine(points, index, length);
    }
    
    function drawLine(route, index, length){
        map.getRoutes({
            origin: [route[index]['start_latitude'], route[index]['start_longitude']],
            destination: [route[index]['end_latitude'], route[index]['end_longitude']],
            travelMode: 'driving',
            callback: function(e){
                var coordinates = new google.maps.LatLng(route[index]['end_latitude'], route[index]['end_longitude']);
                var aux = e[index];
                aux.legs[0].end_location = coordinates;
                all_routes.push(aux);
                if(index < length){
                    drawLine(route, ++index, length);
                }
                else if(index == length){
                    updateInstructions(all_routes);
                    return;
                }
            },
            error: function(){
                setTimeout(function() { drawLine(route, index, length); }, 0);
            }
        });
    }
    
   function updateInstructions(points){
        var all_map_routes = new Array();
        points.forEach(function(routes, key2){
            map_route = {
                start_latitude : routes.legs[0].start_location.lat(),
                start_longitude : routes.legs[0].start_location.lng(),
                end_latitude : routes.legs[0].end_location.lat(),
                end_longitude : routes.legs[0].end_location.lng()
            };
            map_route['overview_polyine'] = routes.overview_polyline;
            var array_steps = new Array();
            routes.legs[0].steps.forEach(function(steps, key){
                var a = new RegExp('<b>', 'g');
                var b = new RegExp('</b>', 'g');
                var c = new RegExp('</div>', 'g');
                var aux2 = steps.instructions.replace(a,'');
                aux2 = aux2.replace(b,'');
                aux2 = aux2.replace(c,'');
                aux2 = aux2.split('<div style="font-size:0.9em">')

                step = {
                    start_latitude : steps.start_location.lat(),
                    start_longitude : steps.start_location.lng(), 
                    end_latitude : steps.end_location.lat(),
                    end_longitude : steps.end_location.lng(), 
                    distance : steps.distance.value,
                    time : steps.duration.value,
                    instructions: aux2[0]
                };
                array_steps.push(step);


            });
            map_route['step'] = array_steps;     
            all_map_routes.push(map_route);
            
            if(key2 == points.length-1){
                var index = 0;
                var length = all_map_routes.length-1;

                if(length >= 0)
                    postData(all_map_routes, index, length);
            }
        });
    }
    
    function postData(routes, index, length){
        $.ajax({
            type: "POST",
            url: "/translate/update_instructions/6",
            data: {
                idroute: idroute,
                map_route: routes[index]
            },
            success: function(e){
                console.log(e);
            },
            complete: function(e){
                console.log(index);
                if(index < length){
                    postData(routes, ++index, length);
                }
                if(index == length){
                    setTimeout(function(){
                        window.location = "/translate/instructions/"+idroute+"/7";
                    },1000);
                }
            },
            error: function (request, status, error) {
                console.log("error - "+request.responseText);
                console.log("error - "+status);
            }
        });
    }


</script>