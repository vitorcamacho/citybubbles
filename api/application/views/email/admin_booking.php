<table style="font-size: 20px; border:1px solid #eeeeee; border-radius:10px; padding: 10px">
    <tr>
        <td><b>Nome</b></td>
        <td><?php echo $book['first_name']." ".$book['last_name'] ?> </td>
    </tr>
    <tr>
        <td><b>Contacto</b></td>
        <td><?php echo $book['contact'] ?> </td>
    </tr>
    <tr>
        <td><b>Email</b></td>
        <td><?php echo $book['email'] ?> </td>
    </tr>
    <tr>
        <td><b>Data Início</b></td>
        <td><?php echo $book['begin_date'] ?> </td>
    </tr>
    <tr>
        <td><b>Inicio</b></td>
        <td><?php echo $book['begin'] ?> </td>
    </tr>
    <tr>
        <td><b>Data Fim</b></td>
        <td><?php echo $book['end_date'] ?> </td>
    </tr>
    <tr>
        <td><b>Fim</b></td>
        <td><?php echo $book['end'] ?> </td>
    </tr>
    <tr>
        <td><b>Número carros</b></td>
        <td><?php echo $book['num_pass'] ?> </td>
    </tr>
    <tr>
        <td colspan="2" style="height: 20px"></td>
    </tr>
    </tr>
    <tr>
        <td colspan="2" style="font-size: 15px;">Efetuado em: <?php echo date("Y-m-d h:i:sa") ?> </td>
    </tr>
</table>