<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php print_r($rbundle->description); ?>" />
        <meta name="keywords" content="city bubbles citybubble citybubbles citybubbles.pt carroseco eco madeira funchal cars rent alugar ferias" />
        <meta name="author" content="Vitor Camacho">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="30 days">
        <link rel="icon" type="image/png" href="/application/assets/img/car.png">
        <title>CityBubbles</title>

        <!-- Bootstrap Core CSS -->
        <link href="/application/assets/css/bootstrap.css" rel="stylesheet">
        <link href="/application/assets/css/bootstrap-glyphicons.css" rel="stylesheet" type="text/css"/>

        <!-- Custom CSS -->
        <link href="/application/assets/css/landing-page.css" rel="stylesheet">
        <link href="/application/assets/css/carousel.css" rel="stylesheet">
        <link href="/application/assets/css/main.css" rel="stylesheet">

        <!-- Fonts -->
        <link href="/application/assets/font-awesome-4.1.0/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>

        <!-- jQuery Version 1.11.0 -->
        <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/application/assets/js/bootstrap.js"></script>

        <!-- Google Map-->
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg5buuARNQozJLurycMyv8M4QukfnMvks&sensor=FALSE&libraries=geometry">></script>
        <script src="/application/assets/js/gmaps.js" type="text/javascript"></script>
        <script src="/application/assets/js/markerAnimate.js" type="text/javascript"></script>


        <script async src="/application/assets/sliderengine/amazingslider.js"></script>
        <link rel="stylesheet" type="text/css" href="/application/assets/sliderengine/amazingslider-0.css">
        <script async src="/application/assets/sliderengine/initslider-0.js"></script>

        <link rel="stylesheet" type="text/css" href="/application/assets/sliderengine/amazingslider-1.css">
        <script async src="/application/assets/sliderengine/initslider-1.js"></script>

    </head>

    <body style="overflow-x: hidden;">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="navbar-header logo" id="header-icon">
            <a class="navbar-brand" href=""><img alt="logo" style="margin-top:-20px;" class="img-responsive" width="130" src="/application/assets/img/page/logo.png"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="navbar-collapse collapse">
            <div class="navbar-header navbar-left">
                <ul class="nav navbar-nav navbar-left">
                    <li id="iniciobt">
                      <a href="#home"><?php echo $rbundle->home; ?></a>
                    </li>
                    <li>
                      <a href="#concept"><?php echo $rbundle->company; ?></a>
                    </li>
                    <!-- <li id="rotasbt">
                      <a href="#routes"><?php echo $rbundle->routes; ?></a>
                    </li> -->
                    <li>
                      <a href="#prices"><?php echo $rbundle->prices; ?></a>
                    </li>
                    <li>
                      <a href="#gallery"><?php echo $rbundle->gallery; ?></a>
                    </li>
                    <li>
                      <a href="#reviews"><?php echo $rbundle->reviews; ?></a>
                    </li>
                    <li>
                      <a href="#contact"><?php echo $rbundle->contact; ?></a>
                    </li>
                    <li>
                      <a href="#booking" data-toggle="modal"><?php echo $rbundle->booking; ?></a>
                    </li>
                </ul>
            </div>
            <div class="navbar-header navbar-right">
                <form class="navbar-form navbar-right languages">
                    <a href="home/lang/pt" style="text-decoration: none; margin-right: 10px;">
                        <img class="img flag <?php if($lang =='pt') echo 'active'; ?>" src="/application/assets/img/page/pt.png" width="40">
                    </a>
                    <a href="home/lang/en">
                        <img class="img flag <?php if($lang =='en') echo 'active'; ?>" src="/application/assets/img/page/en.png" width="40">
                    </a>
                </form>
            </div>

            <div class="navbar-header navbar-right" id="social_icons">
                <form class="navbar-form navbar-right" style="margin-top:4px; margin-bottom:0px;">
                    <a style="margin-right: -15px" href="//www.facebook.com/Citybubbles" class="btn"><img alt="social" style="border-radius: 8px;" class="img-responsive" width="32" src="/application/assets/img/social/facebook.png"></a>
                    <a style="margin-right: -15px" href="//www.tripadvisor.com.br/Attraction_Review-g189167-d3825814-Reviews-CityBubbles-Funchal_Madeira_Madeira_Islands.html" class="btn"><img alt="social" style="border-radius: 8px;" class="img-responsive" width="32" src="/application/assets/img/social/tripadvisor.png"></a>
                    <a style="margin-right: -15px" href="//www.instagram.com/citybubblesmadeira" class="btn"><img alt="social icon" style="border-radius: 8px;" class="img-responsive" width="32" src="/application/assets/img/social/instagram.png"></a>
                    <a style="margin-right: -15px" href="https://www.youtube.com/channel/UCGNolJ8F5ngm2eSRNAMVFUA" class="btn"><img alt="social" style="border-radius: 8px;" class="img-responsive" width="32" src="/application/assets/img/social/youtube-1.png"></a>
                    <a href="https://www.momondo.dk/city-guides/discover-Funchal.13732.guide.ksp" class="btn"><img alt="social" style="border-radius: 8px;" class="img-responsive" width="34" src="/application/assets/img/social/momondo.png"></a>
                </form>
            </div>
        </div>
    </nav>

    <div class="container fill" id="home" >
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
              <?php foreach ($sliders as $key => $slider): ?>
                <div class="item <?php if($key == 0) echo 'active'; ?>">
                    <div class="fill" style="background:url(<?php echo $slider['image']?>) no-repeat left center;"></div>
                </div>
              <?php endforeach; ?>
            </div>
            <div class="pull-center">
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="content-section-a" id="concept">

        <div class="container">

          <?php foreach ($informations as $key=>$information): ?>
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 <?php echo ($key%2 == 0) ? 'pull-left' : 'pull-right'?>">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h2 class="section-heading"><?php echo $information['title']; ?></h2>

                    <p class="lead">
                        <?php echo $information['content']; ?>
                    </p>
                  </div>
                  <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12 <?php echo ($key%2 != 0) ? 'pull-left' : 'pull-right'?>">
                      <img alt="image-<?php echo $information['title']; ?>" class="img-responsive" style="margin-top:80px" src="<?php echo $information['image']; ?>" alt="">
                  </div>
            </div>
            <?php endforeach; ?>
            <br>
        </div>
    </div>


    <section id="services" class="services bg-primary">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1">
                    <h1> <?php echo $rbundle->certificate->header; ?> </h1>
                    <hr class="small">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="service-item">
                                <span class="fa-stack fa-4x">
                                    <img class="img-responsive img-rounded" src="/application/assets/img/page/certified.png" alt="certified">
                                </span>
                                <span class="fa-stack fa-5x">
                                    <img class="img-responsive img-thumbnail" src="/application/assets/img/page/tripadvisor.png" alt="tripadvisor">
                                </span>
                                <h2>
                                    <strong><?php echo $rbundle->certificate->subtitle; ?></strong>
                                </h2>
                                <h3>
                                    <p><?php echo $rbundle->certificate->content; ?></p>
                                </h3>
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- <div class="content-section-b" id="routes" style="text-align: center">
        <div class="row">
            <div class="col-lg-12">
                <h1><?php echo $rbundle->routes ?></h1>
                <hr>
            </div>
            <div class="col-lg-12">
                <div id="map-routes"></div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2" id="over_map" style="overflow: -moz-scrollbars-vertical;overflow-y: scroll;">
                    <ul class="nav nav-pills nav-stacked" id="table_routes">
                        <li class="active" style="opacity: 0.8"><a><h5 style="color:white; font-size: x-large; margin:0px; padding:0px">Lista Rotas</h5></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div> -->

    <div class="content-section-b" id="prices" style="text-align: center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1><?php echo $rbundle->prices; ?></h1>
                    <hr>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <h3>
                                <?php echo $rbundle->pricesContent->title1; ?>
                                <br>
                                <?php echo $rbundle->pricesContent->title2; ?>
                            </h3>
                            <br>
                            <p class="lead">
                                <img alt="price" class="img-responsive" width="200" height="200" style="margin:auto" src="/application/assets/img/page/price.png">
                            </p>
                            <br>
                            <a href="#conditions" data-toggle="modal">
                              <?php echo $rbundle->pricesContent->conditions; ?>
                            </a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="gallery"></div>
    </div>

    <section id="gallery" class="services">
        <div class="container">
            <div class="text-center">
                <h1>Galeria</h1>
                <hr class="small">
                <div class="col-lg-6">
                    <div id="amazingslider-wrapper-1" style="display:block;position:relative;max-width:520px;margin:0px auto 108px;">
                        <div id="amazingslider-1" style="display:block;position:relative;margin:0 auto;">
                            <?php foreach ($photos as $photo): ?>
                                <ul class="amazingslider-slides" style="display:none;">
                                    <li><img alt="gallery" src="<?php echo $photo['url']; ?>"/></li>
                                </ul>
                            <?php endforeach; ?>
                            <?php foreach ($photos as $photo): ?>
                                <ul class="amazingslider-thumbnails" style="display:none;">
                                    <li><img alt="gallery" src="<?php echo $photo['url']; ?>" /></li>
                                </ul>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div id="amazingslider-wrapper-0" style="display:block;position:relative;max-width:520px;margin:0px auto 108px;">
                        <div id="amazingslider-0" style="display:block;position:relative;margin:0 auto;">
                            <ul class="amazingslider-slides" style="display:none;">
                              <?php foreach ($videos as $video): ?>
                                <li><img src="<?php echo $video['preview'] ?>" alt="CityBubbles Madeira" />
                                  <video preload="none" src="<?php echo $video['videoId'] ?>" ></video>
                                </li>
                              <?php endforeach; ?>
                            </ul>
                            <ul class="amazingslider-thumbnails" style="display:none;">
                              <?php foreach ($videos as $video): ?>
                                <li><img src="<?php echo $video['preview'] ?>" alt="CityBubbles Madeira" /></li>
                              <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="reviews"></div>
    </section>


    <hr>
    <section class="services">
        <div class="container" style="width: 100%" >
            <div class="row text-center">
                <h1>Reviews</h1>
                <div class="container fill" id="home" style="height: 400px">
                    <div id="myCarousel2" class="carousel slide">
                        <div class="carousel-inner" >
                          <?php foreach ($reviews as $key => $review): ?>
                            <div class="item <?php if($key == 0) echo 'active'; ?>" style="background-color: #0099cc">
                              <div class="fill">
                                  <div class="jumbotron" style="color:white">
                                      <h1><?php echo $review['title']?></h1>
                                      <div class="col-lg-10 col-lg-offset-1">
                                          <h3><?php echo $review['description']?></h3>
                                      </div>
                                  </div>
                              </div>
                            </div>
                          <?php endforeach; ?>
                        </div>
                        <div class="pull-center">
                            <a class="carousel-control left" href="#myCarousel2" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="carousel-control right" href="#myCarousel2" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="contact"></div>
    </section>

    <hr>
    <section class="services">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h1>Contacto</h1>
                    <hr class="small">
                </div>
            </div>
        </div>
    </section>

    <div id="map-canvas"></div>

    <div class="content-section-b" id="contacts">
        <div class="container">
            <div class="row" style="text-align: center">
                <div class="col-lg-5 col-lg-offset-1 col-md-5 col-md-offset-2 col-sm-5 col-sm-offset-2">
                    <p class="lead">
                        Estª Monumental - Edifício Atlântida <br>
                        nº 185 Touristik Galery <br>
                        9000 Funchal
                    </p>
                    <hr class="section-heading">
                    <p class="lead">
                        <span class="glyphicon glyphicon-envelope"> <a href="mailto:info@citybubbles.pt">info@citybubbles.pt</a></span>
                        <br>
                        <span class="glyphicon glyphicon-phone-alt" style="margin-right: 10px;"> </span> +351 291 782 855
                        <br>
                        <span class="glyphicon glyphicon-earphone" style="margin-right: 10px;"> </span> +351 916 558 805
                    </p>
                </div>
                <div class="col-lg-4 col-sm-3">
                    <p class="lead">
                        <b>10:00 - 20:00</b><br> (Todos os dias)
                    </p>
                </div>
            </div>
            <hr>
        </div>
    </div>

    <!-- /Contact -->

        <div class="content-section-b" id="prices" style="text-align: center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle"
                            style="display:block"
                            data-ad-client="ca-pub-1491844198657364"
                            data-ad-slot="7202359309"
                            data-ad-format="auto">
                        </ins>
                        <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
            </div>
        </div>

    <!-- Footer -->
    <footer style="text-align: center">
        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="color: ">
                    <p class="copyright text-muted small">Copyright &copy; <a href="https://www.linkedin.com/profile/view?id=348789498"> Vitor Camacho</a> 2014. Todos os direitos reservados</p>
                </div>
            </div>
        </div>
    </footer>

    <?php
      $this->load->view("front/book/book", $rbundle);
      $this->load->view("front/conditions/condition", $rbundle);
    ?>

    <script>
        var map;
        $(document).ready(function () {
            map = new GMaps({
                div: '#map-canvas',
                lat: 32.640316,
                lng: -16.923928,
                zoom: 15,
                panControl: false,
                zoomControl: true,
                scrollwheel: false,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.LARGE,
                    position: google.maps.ControlPosition.RIGHT
                }
            });
        
            var office = new google.maps.MarkerImage("/application/assets/img/office_marker.png", null, null, null, new google.maps.Size(64, 64));
            var marker1 = map.addMarker({
                lat: 32.638127,
                lng: -16.930142,
                icon: office,
                infoWindow: {
                    content: "Estª Monumental - Edifício Atlântida <br> nº 185 Touristik Galery <br> 9000 Funchal"
                }
            });
        
            var marker2 = map.addMarker({
                lat: 32.641129,
                lng: -16.916688,
                icon: office,
                infoWindow: {
                    content: "Estª Monumental - Edifício Atlântida <br> nº 185 Touristik Galery <br> 9000 Funchal"
                }
            });
        });

    </script>

    <!-- <script>
        $(function () {
            $('#myCarousel').carousel({
                interval: 4000
            });
            $('#myCarousel2').carousel({
                interval: 10000,
                pause: "none"
            });
        });
        
        $('.nav a').on('click', function () {
            if ($(window).width() < '768') {
                $(".navbar-toggle").click();
            }
        });
        
        $('a').click(function () {
            if ($.attr(this, 'href') === "#myCarousel" || $.attr(this, 'href') === "#myCarousel2" || $.attr(this, 'href') === "#booking" || $.attr(this, 'id') === "amazingslider-wrapper-1" || $.attr(this, 'href') === "#conditions")
                return;
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 800);
            return false;
        });
    </script> -->

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        
        ga('create', 'UA-54228196-1', 'auto');
        ga('send', 'pageview');
    </script>

</body>
</html>

<style>
    #table_routes li a:hover{
        cursor:pointer;
    }
</style>
