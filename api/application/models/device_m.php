<?php

class Device_m extends CI_Model{
    
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('date');
    }
    
    public function insert_device($id = null)
    {
        if($id != null){
            $this->db->query("INSERT INTO device (`iddevice`, `latitude`, `longitude`, `date`) "
                . "VALUES ('$id', 'null', 'null', 'null')");
        }
    }
    
    public function delete_device($id = null) 
    {
        if($id != null)
            $this->db->query("DELETE FROM device WHERE iduser LIKE '$id'");
        
    }
    
    public function get_device($id = null) 
    {
        
        if($id != null)
            $query = $this->db->query("SELECT * FROM device WHERE iddevice LIKE '$id'");
        else
            $query = $this->db->query("SELECT * FROM device");

        return $query->result_array();
    }
    
    
    public function update_device($device,$latitude,$longitude) 
    {
        $datestring = "%d/%m/%Y - %h:%i %a";
        $time = time();
        
        if($device!= null && $latitude!= null &&$longitude != null){
            $query = $this->db->query("SELECT COUNT(*) as count FROM device where iddevice = ".$device."");
            if($query->result_array()[0]['count'] != 0)
                $this->db->query("UPDATE device SET `longitude` = '$longitude', `latitude` = '$latitude', `date` = '".mdate($datestring, $time)."' WHERE iddevice LIKE '$device'");
            else
                $this->db->query("INSERT INTO device (`iddevice`, `latitude`, `longitude`, `date`) VALUES ('$device', 'null', 'null', 'null')");
        }
    }
    
}

