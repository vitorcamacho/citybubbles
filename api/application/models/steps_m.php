<?php

class Steps_m extends CI_Model{
    
         
    public function insert_steps($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO steps (`idsteps`, `start_latitude`, `start_longitude`, `end_latitude`, `end_longitude`, `distance`, `time`, `read`, `encoded_lat_lngs`)"
                . "VALUES (null, '$data[start_latitude]', '$data[start_longitude]', '$data[end_latitude]', '$data[end_longitude]', '$data[distance]', '$data[time]', 0 , '$data[encoded_lat_lngs]')");
        }
        return $this->db->insert_id();
    }
    
    public function update_steps($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE map_routes SET `start_latitude`= '$data[start_latitude]',`start_longitude`= '$data[start_longitude]',`end_latitude`= '$data[end_latitude]',`end_longitude`= '$data[end_longitude]',`distance`= '$data[distance]', `distance`= '$data[time]', '$data[encoded_lat_lngs]' WHERE idsteps LIKE '$data[idsteps]'");
        }
    }
    
    public function set_read($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE map_routes SET `read`= '1' WHERE idsteps LIKE '$data[idsteps]'");
        }
    }
    
    public function get_steps($data = null)
    {
        if($data != null){
            $query = $this->db->query("SELECT * FROM steps "
                                     . "WHERE `start_latitude` LIKE '$data[start_latitude]' AND `start_longitude` LIKE '$data[start_longitude]' "
                                     . "AND `end_latitude` LIKE '$data[end_latitude]' AND `end_longitude` LIKE '$data[end_longitude]'");
        }
        return $query->result_array();
    }
    
    public function delete_steps($data = null)
    {
        if($data != null){
            $query = $this->db->query("DELETE FROM steps "
                                     . "WHERE `start_latitude` LIKE '$data[start_latitude]' AND `start_longitude` LIKE '$data[start_longitude]' "
                                     . "AND `end_latitude` LIKE '$data[end_latitude]' AND `end_longitude` LIKE '$data[end_longitude]'");
        }
    }
    
    public function delete_steps_id($idstep = null)
    {
        if($idstep != null){
            $query = $this->db->query("DELETE FROM steps "
                                     . "WHERE `idsteps` LIKE '$idstep'");
        }
    }
    
    public function get_map_routes_has_steps($idmap_route = null)
    {
        if($idmap_route != null){
            $query = $this->db->query("SELECT * FROM `steps`
                                        LEFT JOIN `map_routes_has_steps` ON `map_routes_has_steps`.`idsteps` = `steps`.`idsteps`
                                        WHERE `map_routes_has_steps`.`idmap_routes` = '$idmap_route'"); 
        }
        return $query->result_array();
    }

}
