<?php

class Pol_m extends CI_Model{
    
         
    public function insert_pol($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO pol (`idpol`, `latitude`, `longitude`, `image`)"
                . "VALUES (null, '$data[latitude]', '$data[longitude]', 'null')");
        }
        return $this->db->insert_id();
    }
    
    public function insert_image($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE pol SET `image`= '$data[image]' WHERE idpol LIKE '$data[idpol]'");
        }
        return $this->db->insert_id();
    }
    
    public function update_pol($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE pol SET `latitude`= '$data[latitude]',`longitude`= '$data[longitude]' WHERE idpol LIKE '$data[idpol]'");
        }
    }
    
    public function delete_pol($data = null)
    {
        if($data != null){
           $this->db->query("DELETE FROM pol WHERE idpol LIKE '$data[idpol]'");
        }
    }
    
    public function get_pol()
    {
        $query = $this->db->query("SELECT  `pol`.`idpol`, `latitude`, `longitude`, `image` as `pol_image`, `pol_info`.`idpol_info` as `pol_info`, `pol_info`.`name` as `pol_name`,  `description` as `pol_description`, `text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name`
                                FROM `pol`
                                INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol`
                                INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info`
                                INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage`");
        
        return $query->result_array();
    }
    
//    public function get_pol_pt($id = null)
    public function get_pol_lang($idpol = null, $idlanguage = null)
    {
        if($idpol == null){
            $query = $this->db->query("SELECT  `pol`.`idpol`, `latitude`, `longitude`, `pol`.`image` as `pol_image`, `pol_info`.`idpol_info`, `pol_info`.`name` as `pol_name`,  `pol_info`.`description` as `pol_description`, `pol_info`.`text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                                    ."FROM `pol`"
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol`" 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info`"
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                                    . "WHERE `language`.`idlanguage` LIKE '$idlanguage'");
        }
        else if ($idlanguage == null)
        {
            $query = $this->db->query("SELECT  `pol`.`idpol`, `latitude`, `longitude`, `pol`.`image` as `pol_image`, `pol_info`.`idpol_info`, `pol_info`.`name` as `pol_name`,  `pol_info`.`description` as `pol_description`, `pol_info`.`text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                                    ."FROM `pol` "
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol` " 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info` "
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                                    . "WHERE `pol`.`idpol` LIKE '$idpol'");
        }
        else
        {
            $query = $this->db->query("SELECT  `pol`.`idpol`, `latitude`, `longitude`, `pol`.`image` as `pol_image`, `pol_info`.`idpol_info`, `pol_info`.`name` as `pol_name`,  `pol_info`.`description` as `pol_description`, `pol_info`.`text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                                    ."FROM `pol` "
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol` " 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info` "
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                                    . "WHERE `pol`.`idpol` LIKE '$idpol' "
                                    . "AND `language`.`idlanguage` LIKE '$idlanguage'");
        }
        return $query->result_array();
    }
    
//    public function get_pol_lang($idpol = null, $idlanguage)
    public function get_pol_route_lang($idroute = null, $idlanguage = null)
    {
        if($idroute == null){
            $query = $this->db->query("SELECT `pol`.`idpol`, `latitude`, `longitude`, `image` as `pol_image`, `pol_info`.`idpol_info` as `idpol_info`, `pol_info`.`name` as `pol_name`,  `description` as `pol_description`, `text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `pol`"
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol`" 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info` "
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                                    ."INNER JOIN `route_has_pol` ON `route_has_pol`.`idpol` =  `pol`.`idpol` "
                                    . "WHERE `language`.`idlanguage` LIKE '$idlanguage'");
        }
        else if ($idlanguage == null)
        {
            $query = $this->db->query("SELECT `pol`.`idpol`, `latitude`, `longitude`, `image` as `pol_image`, `pol_info`.`idpol_info` as `idpol_info`, `pol_info`.`name` as `pol_name`,  `description` as `pol_description`, `text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `pol` "
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol` " 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info` "
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                                    ."INNER JOIN `route_has_pol` ON `route_has_pol`.`idpol` =  `pol`.`idpol` "
                                    . "WHERE `route_has_pol`.`idroute` LIKE '$idroute'");
        }
        else
        {
            $query = $this->db->query("SELECT `pol`.`idpol`, `latitude`, `longitude`, `image` as `pol_image`, `pol_info`.`idpol_info` as `idpol_info`, `pol_info`.`name` as `pol_name`,  `description` as `pol_description`, `text` as `pol_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `pol` "
                                    ."INNER JOIN `pol_info` ON `pol`.`idpol` = `pol_info`.`idpol` " 
                                    ."INNER JOIN `pol_info_has_language` ON `pol_info`.`idpol_info` =  `pol_info_has_language`.`idpol_info` "
                                    ."INNER JOIN `language` ON `pol_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                                    ."INNER JOIN `route_has_pol` ON `route_has_pol`.`idpol` =  `pol`.`idpol` "
                                    . "WHERE `route_has_pol`.`idroute` LIKE '$idroute' "
                                    . "AND `language`.`idlanguage` LIKE '$idlanguage'");
        }
        return $query->result_array();
    }
}
