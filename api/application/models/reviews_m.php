<?php

class Reviews_m extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('language_m');
    }

    public function get($id = null)
    {
      $language = $this->getLanguage();
      if($id) {
        $query = $this->db->query("SELECT * FROM reviews inner join language where reviews.language_idlanguage = language.idlanguage and idreviews = '$id'")->result_array();
      } else {
        $query = $this->db->query("SELECT * FROM reviews inner join language where reviews.language_idlanguage = language.idlanguage")->result_array();
      }
      return $query;
    }

    public function get_by_language()
    {
      $language = $this->getLanguage();
      $query = $this->db->query("SELECT * FROM reviews inner join language where reviews.language_idlanguage = language.idlanguage and language.idlanguage = '$language'")->result_array();

      return $query;
    }

    public function create_reviews($data = null)
    { print_r($data);
      if($data != null){
          return $this->db->query(
          sprintf("INSERT INTO reviews (`idreviews`, `title`, `description`, `language_idlanguage`) VALUES (null, '%s', '%s', %s )",
            $data['title'],
            $data['description'],
            $data['language']));
      }
    }

    public function edit_reviews($data = null)
    {
      return $this->db->query(
        sprintf("UPDATE reviews SET `title` = '%s', `description` = '%s' WHERE `idreviews` = '%s'",
          $data['title'],
          $data['description'],
          $data['idreviews'])
      );
    }

    public function delete($id = null)
    {
        if($id != null){
           $this->db->query("DELETE FROM reviews WHERE idreviews LIKE '$id'");
        }
    }

    private function getLanguage($lang = null) {
      if($lang) {
        $chooseLang = $lang;
      } else {
        $chooseLang = $this->session->userdata('lang') ? $this->session->userdata('lang') : "PT";
      }
      return $this->language_m->get_language_by_name($chooseLang)[0]['idlanguage'];
    }
}
