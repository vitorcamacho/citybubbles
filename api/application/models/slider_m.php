<?php

class Slider_m extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('language_m');
    }

    public function get($id = null)
    {
      $language = $this->getLanguage();
      if($id) {
        $query = $this->db->query("SELECT * FROM slider inner join language where slider.language_idlanguage = language.idlanguage and idslider = '$id'")->result_array();
      } else {
        $query = $this->db->query("SELECT * FROM slider inner join language where slider.language_idlanguage = language.idlanguage")->result_array();
      }
      return $query;
    }

    public function get_by_language()
    {
      $language = $this->getLanguage();
      $query = $this->db->query("SELECT * FROM slider inner join language where slider.language_idlanguage = language.idlanguage and language.idlanguage = '$language'")->result_array();

      return $query;
    }

    public function create_slider($data = null)
    {
      if($data != null){
          return $this->db->query("INSERT INTO slider (`idslider`, `image`, `language_idlanguage`)"
              . " VALUES (null, '$data[image]', $data[language] )");
      }
    }

    public function edit_slider($data = null)
    {
      if($data != null && isset($data['image'])){
        return $this->db->query("UPDATE slider SET `image` = '$data[image]' WHERE `idslider` = '$data[idslider]'");
      }
    }

    public function delete($id = null)
    {
        if($id != null){
           $this->db->query("DELETE FROM slider WHERE idslider LIKE '$id'");
        }
    }

    private function getLanguage($lang = null) {
      if($lang) {
        $chooseLang = $lang;
      } else {
        $chooseLang = $this->session->userdata('lang') ? $this->session->userdata('lang') : "PT";
      }
      return $this->language_m->get_language_by_name($chooseLang)[0]['idlanguage'];
    }
}
