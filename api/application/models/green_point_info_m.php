<?php

class Green_point_info_m extends CI_Model{
    
    public function insert_green_point_info($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO green_point_info (`idgreen_point_info`, `text`)"
                . " VALUES (null, '$data[text]')");
        }
    }
    
    public function get_green_points_info($language=null)
    {
        if($language!=null){
            $query = $this->db->query("SELECT * FROM green_point_info
                                   INNER JOIN `green_point_info_has_language` ON `green_point_info_has_language`.`idgreen_point_info` LIKE `green_point_info`.`idgreen_point_info`
                                   INNER JOIN `language` ON `language`.`idlanguage` LIKE `green_point_info_has_language`.`idlanguage` 
                                   WHERE `language`.`idlanguage` LIKE '$language'");
        }
        else
        {
            $query = $this->db->query("SELECT * FROM green_point_info
                                   INNER JOIN `green_point_info_has_language` ON `green_point_info_has_language`.`idgreen_point_info` LIKE `green_point_info`.`idgreen_point_info`
                                   INNER JOIN `language` ON `language`.`idlanguage` LIKE `green_point_info_has_language`.`idlanguage` ");
        }
        return $query->result_array();
    }
    
    public function update_gpoint_info($data = null)
    {
        if($data != null){
            $text = $data['text'];
            $query = $this->db->query("UPDATE green_point_info SET `text`= '$text' WHERE idgreen_point_info LIKE '$data[idlanguage]'");
        }
        return $query->result_array();
    }
}
