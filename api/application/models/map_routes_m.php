<?php

class Map_routes_m extends CI_Model{
    
         
    public function insert_map_routes($data = null)
    {
        if($data != null){
            $overview_polyline = $data['overview_polyine'];
            $this->db->query("INSERT INTO map_routes (`idmap_routes`, `start_latitude`, `start_longitude`, `end_latitude`, `end_longitude`, `overview_polyline`)"
                . "VALUES (null, '$data[start_latitude]', '$data[start_longitude]', '$data[end_latitude]', '$data[end_longitude]', '$overview_polyline' )");
        }
        return $this->db->insert_id();
    }
    
    public function update_map_routes($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE map_routes SET `start_latitude`= '$data[start_latitude]',`start_longitude`= '$data[start_longitude]',`end_latitude`= '$data[end_latitude]',`end_longitude`= '$data[end_longitude]',`overview_polyline`= '$data[overview_polyline]' WHERE idmap_routes LIKE '$data[idmap_routes]'");
        }
    }
    
    public function get_map_routes($data = null, $idroute = null)
    {
        if($data != null && $idroute != null){
            $query = $this->db->query("SELECT * FROM `map_routes`
                                    LEFT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idmap_routes` = `map_routes`.`idmap_routes` 
                                    WHERE `start_latitude` LIKE '$data[start_latitude]' AND `start_longitude` LIKE '$data[start_longitude]'
                                    AND `end_latitude` LIKE '$data[end_latitude]' AND `end_longitude` LIKE '$data[end_longitude]' AND `idroute` LIKE '$idroute'");
        }
        return $query->result_array();
    }
    
    public function get_map_routes_id($data = null, $idroute = null)
    {
        if($data != null && $idroute != null){
            $query = $this->db->query("SELECT * FROM map_routes "
                                     . "LEFT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idmap_routes` = `map_routes`.`idmap_routes`"
                                     . "WHERE `start_latitude` LIKE '$data[start_latitude]' AND `start_longitude` LIKE '$data[start_longitude]' "
                                     . "AND `end_latitude` LIKE '$data[end_latitude]' AND `end_longitude` LIKE '$data[end_longitude]' "
                                     . "AND idroute LIKE '$idroute'");
        }
        return $query->result_array();
    }
    
    public function get_map_routes_instructions($idroute = null)
    {
        if($idroute != null){
            $query = $this->db->query("SELECT `steps_has_instructions`.`idinstructions` FROM `map_routes`
                                        LEFT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idmap_routes` = `map_routes`.`idmap_routes`
                                        RIGHT JOIN `map_routes_has_steps` ON `map_routes`.`idmap_routes` = `map_routes_has_steps`.`idmap_routes`
                                        RIGHT JOIN `steps` ON `map_routes_has_steps`.`idsteps` = `steps`.`idsteps` 
                                        RIGHT JOIN `steps_has_instructions` ON `steps_has_instructions`.`idsteps` = `steps`.`idsteps`
                                        WHERE `route_has_map_routes`.`idroute` = '$idroute'");
        }
        return $query->result_array();
    }
    
    public function get_map_routes_steps($idroute = null)
    {
        if($idroute != null){
            $query = $this->db->query("SELECT `steps`.`idsteps` FROM `map_routes`
                                        LEFT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idmap_routes` = `map_routes`.`idmap_routes`
                                        RIGHT JOIN `map_routes_has_steps` ON `map_routes`.`idmap_routes` = `map_routes_has_steps`.`idmap_routes`
                                        RIGHT JOIN `steps` ON `map_routes_has_steps`.`idsteps` = `steps`.`idsteps` 
                                        WHERE `route_has_map_routes`.`idroute` = '$idroute'"); 
        }
        return $query->result_array();
    }
    
    
    public function get_map_routes_maproutes($idroute = null)
    {
        if($idroute != null){
            $query = $this->db->query("SELECT * FROM `map_routes`
                                        LEFT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idmap_routes` = `map_routes`.`idmap_routes`
                                        WHERE `route_has_map_routes`.`idroute` = '$idroute'"); 
        }
        return $query->result_array();
    }
    
    public function delete_map_routes($data = null)
    {
        if($data != null){
            $query = $this->db->query("DELETE FROM map_routes "
                                     . "WHERE `start_latitude` LIKE '$data[start_latitude]' AND `start_longitude` LIKE '$data[start_longitude]' "
                                     . "AND `end_latitude` LIKE '$data[end_latitude]' AND `end_longitude` LIKE '$data[end_longitude]'");
        }
    }
    
    public function delete_map_routes_id($idmap_routes = null)
    {
        if($idmap_routes != null){
            $query = $this->db->query("DELETE FROM map_routes "
                                     . "WHERE `idmap_routes` LIKE '$idmap_routes'");            
        }
    }
}
