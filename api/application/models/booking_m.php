<?php

class Booking_m extends CI_Model {
    public function insert_booking($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO booking (`idbooking`, `first_name`, `last_name`, `age`, `contact`, `email`, `begin_date`, `end_date`, `begin`, `end`, `num_pass`, `read`)"
                . " VALUES (null, '$data[first_name]', '$data[last_name]', '$data[age]', '$data[contact]', '$data[email]', '$data[begin_date]', '$data[end_date]', '$data[begin]', '$data[end]', '$data[num_pass]', '0')");
        }
    }
    
    public function delete_booking($id = null)
    {
        if($id != null){
           $this->db->query("DELETE FROM booking WHERE idbooking LIKE '$id'");
        }
    }
    
    public function read_booking($id = null)
    {
        if($id != null){
           $this->db->query("UPDATE booking SET `read`= '1' WHERE idbooking LIKE '$id'");
        }
    }
    
    public function get_booking()
    {
        $query = $this->db->query("SELECT * FROM `booking` ORDER BY `begin_date` DESC");                             
        return $query->result_array(); 
    }
    
    public function get_unread_booking()
    {
        $query = $this->db->query("SELECT * FROM `booking` WHERE `read` = '0'");                             
        return $query->result_array(); 
    }
}
