<?php

class Information_m extends CI_Model{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('language_m');
    }

    public function get($id = null)
    {
      $language = $this->getLanguage();
      if($id) {
        $query = $this->db->query("SELECT * FROM information inner join language where information.language_idlanguage = language.idlanguage and idinformation = $id")->result_array();
      } else {
        $query = $this->db->query("SELECT * FROM information inner join language where information.language_idlanguage = language.idlanguage")->result_array();
      }
      return $query;
    }

    public function get_by_language()
    {
      $language = $this->getLanguage();
      $query = $this->db->query("SELECT * FROM information inner join language where information.language_idlanguage = language.idlanguage and language_idlanguage = $language")->result_array();

      return $query;
    }

    public function create_info($data = null)
    {
      if($data != null){
          return $this->db->query("INSERT INTO information (`idinformation`, `title`, `image`, `content`, `language_idlanguage`)"
              . " VALUES (null, '".$data['title']."', '$data[image]', '".$data['content']."', $data[language] )");
      }
    }

    public function edit_info($data = null)
    {
      if($data != null && !isset($data['image'])){
        return $this->db->query("UPDATE information SET `title` = '".$data['title']."', `content` = '".$data['content']."' WHERE `idinformation` = '$data[idinformation]'");
      } else {
        return $this->db->query("UPDATE information SET `title` = '".$data['title']."', `content` = '".$data['content']."', `image` = '$data[image]' WHERE `idinformation` = '$data[idinformation]'");
      }
    }

    public function delete($id = null)
    {
        if($id != null){
           $this->db->query("DELETE FROM information WHERE idinformation LIKE '$id'");
        }
    }

    private function getLanguage($lang = null) {
      if($lang) {
        $chooseLang = $lang;
      } else {
        $chooseLang = $this->session->userdata('lang') ? $this->session->userdata('lang') : "PT";
      }
      return $this->language_m->get_language_by_name($chooseLang)[0]['idlanguage'];
    }
}
