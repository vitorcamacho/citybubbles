<?php

class Instructions_m extends CI_Model{
    
         
    public function insert_instructions($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO instructions (`idinstructions`, `text`)"
                . "VALUES (null, '$data[instructions]')");
        }
        return $this->db->insert_id();
    }
    
    public function insert_empty_instructions($data = null)
    {
        $this->db->query("INSERT INTO instructions (`idinstructions`, `text`)"
            . "VALUES (null, '')");
            
        return $this->db->insert_id();
    }
    
    public function update_instructions($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE instructions SET `text`= '$data[text]' WHERE idinstructions LIKE '$data[idinstructions]'");
        }
    }
    
    public function delete_instructions_id($idinstructions = null)
    {
        if($idinstructions != null){
            $query = $this->db->query("DELETE FROM instructions "
                                     . "WHERE `idinstructions` LIKE '$idinstructions'");
        }
    }
    
    public function get_instructions($idstep = null, $language = null)
    {
          if($idstep != null && $language != null){
                $query = $this->db->query("SELECT `instructions`.`idinstructions`, `instructions`.`text`, `name` as 'language' FROM `instructions`
                                            RIGHT JOIN `steps_has_instructions` ON `steps_has_instructions`.`idinstructions` = `instructions`.`idinstructions`
                                            LEFT JOIN `instructions_has_language` ON `instructions_has_language`.`idinstructions` = `instructions`.`idinstructions`
                                            INNER JOIN `language` ON `language`.`idlanguage` = `instructions_has_language`.`idlanguage`
                                            WHERE `steps_has_instructions`.`idsteps` = '$idstep' 
                                            AND `language`.`idlanguage` like '$language'"); 
        }
        return $query->result_array(); 
    }
}
