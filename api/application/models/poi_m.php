<?php

class Poi_m extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert_poi($data = null) {
        if ($data != null) {
            $this->db->query("INSERT INTO poi (`idpoi`, `latitude`, `longitude`, `image`)"
                    . "VALUES (null, '$data[latitude]', '$data[longitude]', 'null')");
        }
        return $this->db->insert_id();
    }

    public function insert_image($data = null) {
        if ($data != null) {
            $this->db->query("UPDATE poi SET `image`= '$data[image]' WHERE idpoi LIKE '$data[idpoi]'");
        }
        return $this->db->insert_id();
    }

    public function update_poi($data = null) {
        if ($data != null) {
            $this->db->query("UPDATE poi SET `latitude`= '$data[latitude]',`longitude`= '$data[longitude]' WHERE idpoi LIKE '$data[idpoi]'");
        }
    }

    public function delete_poi($data = null) {
        if ($data != null) {
            $this->db->query("DELETE FROM poi WHERE idpoi LIKE '$data[idpoi]'");
        }
    }

    public function get_poi($idroute = null) {
        if ($idroute == null) {
            $query = $this->db->query("SELECT  `poi`.`idpoi`, `latitude`, `longitude`, `image` as `poi_image`, `poi_info`.`idpoi_info` as `poi_info`, `poi_info`.`name` as `poi_name`,  `description` as `poi_description`, `text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                    . "FROM `poi`"
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi`"
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info`"
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage`");
        } else {
            $query = $this->db->query("SELECT  `poi`.`idpoi`, `latitude`, `longitude`, `image` as `poi_image`, `poi_info`.`idpoi_info` as `poi_info`, `poi_info`.`name` as `poi_name`,  `description` as `poi_description`, `text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                    . "FROM `poi` "
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi` "
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                    . "WHERE `poi`.`idpoi` LIKE '$idroute'");
        }
        return $query->result_array();
    }

//    public function get_poi_pt($id = null)
    public function get_poi_lang($idpoi = null, $idlanguage = null) {
        if ($idpoi == null) {
            $query = $this->db->query("SELECT  `poi`.`idpoi`, `latitude`, `longitude`, `poi`.`image` as `poi_image`, `poi_info`.`idpoi_info`, `poi_info`.`name` as `poi_name`,  `poi_info`.`description` as `poi_description`, `poi_info`.`text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                    . "FROM `poi`"
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi`"
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info`"
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                    . "WHERE `language`.`idlanguage` LIKE '$idlanguage'");
        } else if ($idlanguage == null) {
            $query = $this->db->query("SELECT  `poi`.`idpoi`, `latitude`, `longitude`, `poi`.`image` as `poi_image`, `poi_info`.`idpoi_info`, `poi_info`.`name` as `poi_name`,  `poi_info`.`description` as `poi_description`, `poi_info`.`text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                    . "FROM `poi` "
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi` "
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                    . "WHERE `poi`.`idpoi` LIKE '$idpoi'");
        } else {
            $query = $this->db->query("SELECT  `poi`.`idpoi`, `latitude`, `longitude`, `poi`.`image` as `poi_image`, `poi_info`.`idpoi_info`, `poi_info`.`name` as `poi_name`,  `poi_info`.`description` as `poi_description`, `poi_info`.`text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name`"
                    . "FROM `poi` "
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi` "
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                    . "WHERE `poi`.`idpoi` LIKE '$idpoi' "
                    . "AND `language`.`idlanguage` LIKE '$idlanguage'");
        }
        return $query->result_array();
    }

//    public function get_poi_lang($idpoi = null, $idlanguage)
    public function get_poi_route_lang($idroute = null, $idlanguage = null) {
        if ($idroute == null) {
            $query = $this->db->query("SELECT `poi`.`idpoi`, `latitude`, `longitude`, `image` as `poi_image`, `poi_info`.`idpoi_info` as `idpoi_info`, `poi_info`.`name` as `poi_name`,  `description` as `poi_description`, `text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                    . "FROM `poi`"
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi`"
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                    . "INNER JOIN `route_has_poi` ON `route_has_poi`.`idpoi` =  `poi`.`idpoi` "
                    . "WHERE `language`.`idlanguage` LIKE '$idlanguage'");
        } else if ($idlanguage == null) {
            $query = $this->db->query("SELECT `poi`.`idpoi`, `latitude`, `longitude`, `image` as `poi_image`, `poi_info`.`idpoi_info` as `idpoi_info`, `poi_info`.`name` as `poi_name`,  `description` as `poi_description`, `text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                    . "FROM `poi` "
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi` "
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                    . "INNER JOIN `route_has_poi` ON `route_has_poi`.`idpoi` =  `poi`.`idpoi` "
                    . "WHERE `route_has_poi`.`idroute` LIKE '$idroute'");
        } else {
            $query = $this->db->query("SELECT `poi`.`idpoi`, `latitude`, `longitude`, `image` as `poi_image`, `poi_info`.`idpoi_info` as `idpoi_info`, `poi_info`.`name` as `poi_name`,  `description` as `poi_description`, `text` as `poi_text`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                    . "FROM `poi` "
                    . "INNER JOIN `poi_info` ON `poi`.`idpoi` = `poi_info`.`idpoi` "
                    . "INNER JOIN `poi_info_has_language` ON `poi_info`.`idpoi_info` =  `poi_info_has_language`.`idpoi_info` "
                    . "INNER JOIN `language` ON `poi_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                    . "INNER JOIN `route_has_poi` ON `route_has_poi`.`idpoi` =  `poi`.`idpoi` "
                    . "WHERE `route_has_poi`.`idroute` LIKE '$idroute' "
                    . "AND `language`.`idlanguage` LIKE '$idlanguage'");
        }
        return $query->result_array();
    }

}
