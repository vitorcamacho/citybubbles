<?php

class Route_m extends CI_Model{
    
         
    public function insert_route($data = null)
    {
        if($data != null){
            $this->db->query("INSERT INTO route (`idroute`, `image`)"
                . "VALUES (null, '')");
        }
        return $this->db->insert_id();
    }
    
    public function insert_image($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE route SET `image`= '$data[image]' WHERE idroute LIKE '$data[idroute]'");
        }
        return $this->db->insert_id();
    }
    
    public function update_route($data = null)
    {
        if($data != null){
           $this->db->query("UPDATE route SET `iamge`= '$data[image]' WHERE idroute LIKE '$data[idroute]'");
        }
    }
    
    public function delete_route($id = null)
    {
        if($id != null){
           $this->db->query("DELETE FROM route WHERE idroute LIKE '$id'");
        }
    }
    
    public function get_routes($id = null)
    {
        if($id == null){
            $query = $this->db->query("SELECT `route`.`idroute` FROM `route` ");
        }
        
        return $query->result_array();
    }
    
    public function get_route($id = null)
    {
        if($id == null){
            $query = $this->db->query("SELECT `route`.`idroute`, `image`, `route_info`.`idroute_info`, `route_info`.`name` as `info_name`,  `description`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `route` "
                                    ."INNER JOIN `route_info` ON `route`.`idroute` = `route_info`.`idroute` " 
                                    ."INNER JOIN `route_info_has_language` ON `route_info`.`idroute_info` =  `route_info_has_language`.`idroute_info` "
                                    ."INNER JOIN `language` ON `route_info_has_language`.`idlanguage` =  `language`.`idlanguage`");
        }
        else
        {
            $query = $this->db->query("SELECT `route`.`idroute`, `image`, `route_info`.`idroute_info`, `route_info`.`name` as `info_name`,  `description`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `route` "
                                    ."INNER JOIN `route_info` ON `route`.`idroute` = `route_info`.`idroute` " 
                                    ."INNER JOIN `route_info_has_language` ON `route_info`.`idroute_info` =  `route_info_has_language`.`idroute_info` "
                                    ."INNER JOIN `language` ON `route_info_has_language`.`idlanguage` =  `language`.`idlanguage` "
                                    ."WHERE `route`.`idroute` LIKE '$id'");
        }
        return $query->result_array();
    }
    
//    get_route_pt
    public function get_route_lang($idroute = null, $idlanguage = null)
    {
        if($idroute == null){
            $query = $this->db->query("SELECT `route`.`idroute`, `image`, `route_info`.`idroute_info`, `route_info`.`name` as `info_name`,  `description`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `route` "
                                    ."INNER JOIN `route_info` ON `route`.`idroute` = `route_info`.`idroute` " 
                                    ."INNER JOIN `route_info_has_language` ON `route_info`.`idroute_info` =  `route_info_has_language`.`idroute_info` "
                                    ."INNER JOIN `language` ON `route_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                                    ."WHERE `language`.`idlanguage` LIKE '$idlanguage'");
        }
        else if ($idlanguage == null) {
            $query = $this->db->query("SELECT `route`.`idroute`, `image`, `route_info`.`idroute_info`, `route_info`.`name` as `info_name`,  `description`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `route` "
                                    ."INNER JOIN `route_info` ON `route`.`idroute` = `route_info`.`idroute` " 
                                    ."INNER JOIN `route_info_has_language` ON `route_info`.`idroute_info` =  `route_info_has_language`.`idroute_info` "
                                    ."INNER JOIN `language` ON `route_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                                    ."WHERE `route`.`idroute` LIKE '$idroute'");
        }
        else
        {
            $query = $this->db->query("SELECT `route`.`idroute`, `image`, `route_info`.`idroute_info`, `route_info`.`name` as `info_name`,  `description`, `language`.`idlanguage`, `language`.`name` as `language_name` "
                                    ."FROM `route` "
                                    ."INNER JOIN `route_info` ON `route`.`idroute` = `route_info`.`idroute` " 
                                    ."INNER JOIN `route_info_has_language` ON `route_info`.`idroute_info` =  `route_info_has_language`.`idroute_info` "
                                    ."INNER JOIN `language` ON `route_info_has_language`.`idlanguage` =  `language`.`idlanguage`"
                                    . "WHERE `route`.`idroute` LIKE '$idroute' "
                                    . "AND `language`.`idlanguage` LIKE '$idlanguage'");
        }
        return $query->result_array();
    }
    
    public function get_route_lat($idroute = null)
    {
        if($idroute != null){
            $query = $this->db->query("SELECT * FROM `route`
                                    RIGHT JOIN `route_has_map_routes` ON `route_has_map_routes`.`idroute` = `route`.`idroute`
                                    RIGHT JOIN `map_routes` ON `map_routes`.`idmap_routes` = `route_has_map_routes`.`idmap_routes`
                                    WHERE `route`.`idroute` = '$idroute'");
        }
        return $query->result_array();
    }
    
}
