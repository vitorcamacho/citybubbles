var routes, car, current_route, route_pois, stop = false;
var all_routes = new Array();
var open_info = false;
var map_routes = new GMaps({
    div: '#map-routes',
    lat: 32.640127,
    lng: -16.930142,
    zoom: 15,
    panControl: false,
    zoomControl: true,
    scrollwheel: false,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.RIGHT
    }
});
var myIcon = new google.maps.MarkerImage("application/assets/img/map/car.png", null, null, null, new google.maps.Size(40, 40));
var marker = new google.maps.Marker({
    icon: myIcon
});

car = map_routes.addMarker({
    lat: 32.638127,
    lng: -16.930142,
    icon: myIcon,
    infoWindow: {
        content: "<div style='height:100px;'><h3>Escolha uma rota no menu à esquerda para ver a animação.</h3></div>"
    }
});
if (car.infoWindow != undefined)
    (car.infoWindow).open(map_routes.map, car);

$(document).ready(function () {
    $.ajax({
        url: "home/get_routes/1",
        dataType: "json",
        success: function (data) {
            var info = eval(data);
            routes = info;
        },
        complete: function () {
            drawRoutesNames(routes);
        },
        error: function (request, status, error) {
            console.log("error - " + request.responseText);
            console.log("error - " + status);
        }
    });
});

function drawRoutesNames(points) {
    points.forEach(function (point, key) {
        if (key > 0) {
            $('#table_routes').append("<li><a style='font-size: medium;' id=" + point['idroute'] + ">" + point['info_name'] + "</a></li>");
        }
    });
}

$(document).on('click', '#table_routes li a', function (e) {
    stop = true;
    if (car.infoWindow != undefined)
        (car.infoWindow).close();
    e.preventDefault();
    map_routes.removeMarkers();
    map_routes.removePolylines();
    car = null;
    current_route = null;
    route_pois = new Array();
    all_routes = new Array();
    var id = this.id;
    setTimeout(function () {
        getRouteMap(id);
    }, 1000);
});

function getRouteMap(id) {
    routes.forEach(function (route, key) {
        if (route['idroute'] == id) {
            drawRoute(routes[key]);
            drawPois(routes[key]);
        }
    });
}

function drawRoute(points) {
    points['map_routes'].forEach(function (point, key) {
        var path = google.maps.geometry.encoding.decodePath(point['overview_polyline']);
        path['start_latitude'] = point['start_latitude'];
        path['start_longitude'] = point['start_longitude'];
        path['end_latitude'] = point['end_latitude'];
        path['end_longitude'] = point['end_longitude'];
        all_routes.push(path);

        if (key == 0) {
            var image = new google.maps.MarkerImage("application/assets/img/map/car.png", null, null, null, new google.maps.Size(30, 30));
            car = map_routes.addMarker({
                lat: path[0].lat(),
                lng: path[0].lng(),
                icon: image
            });
            map_routes.panTo(new google.maps.LatLng(path[0].lat(), path[0].lng()));
        }
        map_routes.drawPolyline({
            path: path,
            strokeColor: '#131540',
            strokeOpacity: 0.2,
            strokeWeight: 6
        });
        if (key == points['map_routes'].length - 1) {
            stop = false;
            map_routes.setZoom(16);
            setTimeout(function () {
                prepareAnimation(0)
            }, 2000);
        }
    });
}

function drawPois(points) {
    var poi_icon = new google.maps.MarkerImage("/application/assets/img/map/poi_blue.png", null, null, null, new google.maps.Size(32, 32));
    points['poi'].forEach(function (poi) {
        poi['read'] = 0;
        route_pois.push(poi);
        map_routes.addMarker({
            lat: poi['latitude'],
            lng: poi['longitude'],
            icon: poi_icon,
            customInfo: poi,
            infoWindow: {
                content: "<h3>" + poi['poi_name'] + "</h3><br>" + poi['poi_description'] + "<br><br>"
            }
        });
    });
}

function prepareAnimation(index) {
    if (stop)
        return;

    if (all_routes[index] != undefined) {
        current_route = all_routes[index];
        moveNextRoute(current_route, index, 0);
    }
}

function moveNextRoute(route, index, index2) {
    if (stop)
        return;
    if (route.length - 1 == index2) {
        prepareAnimation(++index);
    }
    else
        anime(route, index, index2);
}

function anime(route, index, index2) {
    if (stop)
        return;
    var coordinates = new google.maps.LatLng(route[index2].lat(), route[index2].lng());
    var distance_point = google.maps.geometry.spherical.computeDistanceBetween(car.position, coordinates);
    if (index2 % 2 == 0)
        map_routes.panTo(coordinates);
    car.animateTo(coordinates, {duration: distance_point * 5});
    setTimeout(function () {
        map_routes.markers.forEach(function (poi, key) {
            var poi_coordinate = new google.maps.LatLng(poi.position.lat(), poi.position.lng());
            var distance = google.maps.geometry.spherical.computeDistanceBetween(car.position, poi_coordinate);
            if (distance < 40 && poi['customInfo'] != undefined && poi['customInfo']['read'] == 0) {
                var time = 4000;
                open_info = true;
                poi['customInfo']['read'] = 1;
                (poi.infoWindow).open(map_routes.map, poi);
                setTimeout(function () {
                    (poi.infoWindow).close();
                    open_info = false;
                    moveNextRoute(route, index, ++index2);
                }, time);
            }
            if (key == route_pois.length - 1 && open_info == false)
                moveNextRoute(route, index, ++index2);
        });
    }, distance_point * 5);
}
