$(function () {
    $('#myCarousel').carousel({
        interval: 4000
    });
    $('#myCarousel2').carousel({
        interval: 10000,
        pause: "none"
    });
});

$('.nav a').on('click', function () {
    if ($(window).width() < '768') {
        $(".navbar-toggle").click();
    }
});

$('a').click(function () {
    if ($.attr(this, 'href') === "#myCarousel" || $.attr(this, 'href') === "#myCarousel2" || $.attr(this, 'href') === "#booking" || $.attr(this, 'id') === "amazingslider-wrapper-1" || $.attr(this, 'href') === "#conditions")
        return;
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 800);
    return false;
});
