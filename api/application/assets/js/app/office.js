var map;
$(document).ready(function () {
    map = new GMaps({
        div: '#map-canvas',
        lat: 32.640316,
        lng: -16.923928,
        zoom: 15,
        panControl: false,
        zoomControl: true,
        scrollwheel: false,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT
        }
    });

    var office = new google.maps.MarkerImage("application/assets/img/office_marker.png", null, null, null, new google.maps.Size(64, 64));
    var marker1 = map.addMarker({
        lat: 32.638127,
        lng: -16.930142,
        icon: office,
        infoWindow: {
            content: "Estª Monumental - Edifício Atlântida <br> nº 185 Touristik Galery <br> 9000 Funchal"
        }
    });

    var marker2 = map.addMarker({
        lat: 32.641129,
        lng: -16.916688,
        icon: office,
        infoWindow: {
            content: "Estª Monumental - Edifício Atlântida <br> nº 185 Touristik Galery <br> 9000 Funchal"
        }
    });
});
