import type { ComponentProps } from 'react';
import cx from 'classnames';
import { useState, useRef } from 'react';
import { useClickAway } from 'react-use';
import { Link } from '@remix-run/react';

type LinkRef = { label: string; to: string; highlight?: boolean };
const links: Record<string, LinkRef> = {
  media: {
    label: 'Media',
    to: '/media',
  },
  testimonial: {
    label: 'Testimonial',
    to: '/testimonial',
  },
  pricing: {
    label: 'Pricing',
    to: '/pricing',
  },
  about: {
    label: 'About us',
    to: '/about',
  },
  booking: {
    label: 'Book now',
    to: '/booking',
    highlight: true,
  },
};

const Logo = () => (
  <Link to="/" prefetch="intent" className="inline-flex items-center">
    <h1 className="text-3xl font-semibold text-gray-700">CityBubbles</h1>
  </Link>
);

type MenuItemProps = ComponentProps<typeof Link> & {
  label: string;
  highlight?: boolean;
};
const MenuItem: React.VFC<MenuItemProps> = ({ label, to, highlight, ...linkProps }) => (
  <Link
    key={label}
    to={to}
    prefetch="intent"
    className={cx('block rounded-lg border border-transparent py-2 px-1', {
      'text-green-600 outline-none hover:border-green-700 hover:text-green-800': !highlight,
      'animate-pulse bg-green-700 text-white hover:bg-green-800': highlight,
    })}
    {...linkProps}
  >
    {label}
  </Link>
);

export const NavBarLinks: React.VFC<{
  show: boolean;
  onClick?: MenuItemProps['onClick'];
}> = ({ show, onClick }) => (
  <ul
    className={cx(
      'w-height fixed inset-x-0 top-14 w-full flex-col gap-y-2 bg-gray-100 p-4 transition-all md:relative md:top-0 md:mt-0 md:flex md:w-auto md:flex-row md:gap-x-4',
      show ? 'flex' : 'hidden'
    )}
  >
    {Object.entries(links).map(([_, menu]) => (
      <li key={menu.label}>
        <MenuItem onClick={onClick} {...menu} />
      </li>
    ))}
  </ul>
);

const CollapseButton: React.VFC<ComponentProps<'button'>> = (props) => (
  <button
    data-collapse-toggle="mobile-menu"
    type="button"
    className="ml-3 inline-flex items-center rounded-lg p-2 text-sm text-green-700 hover:bg-green-50 focus:outline-none focus:ring-2 focus:ring-green-600 md:hidden"
    aria-controls="mobile-menu"
    aria-expanded="false"
    {...props}
  >
    <span className="sr-only">Open main menu</span>
    <svg
      className="h-6 w-6"
      fill="currentColor"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
        clipRule="evenodd"
      ></path>
    </svg>
    <svg
      className="hidden h-6 w-6"
      fill="currentColor"
      viewBox="0 0 20 20"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
        clipRule="evenodd"
      ></path>
    </svg>
  </button>
);

const Navbar: React.VFC = () => {
  const [navbarOpen, setNavbarOpen] = useState(false);
  const triggerMenu = () => setNavbarOpen((state) => !state);
  const navRef = useRef<HTMLElement>(null);

  useClickAway(navRef, () => {
    if (navbarOpen) triggerMenu();
  });

  return (
    <nav ref={navRef} className="relative border-gray-200 px-4 py-2.5">
      <div className="container mx-auto flex flex-wrap items-center justify-between">
        <Logo />
        <CollapseButton onClick={triggerMenu} />
        <NavBarLinks onClick={triggerMenu} show={navbarOpen} />
      </div>
    </nav>
  );
};

export default Navbar;
