import type { MetaFunction } from '@remix-run/node';
import { Links, LiveReload, Meta, Outlet, Scripts, ScrollRestoration } from '@remix-run/react';
import Navbar, { NavBarLinks } from './components/Navbar/Navbar';

import tailwind from './tailwind.css';

export const meta: MetaFunction = () => ({
  charset: 'utf-8',
  title: 'New Remix App',
  viewport: 'width=device-width,initial-scale=1',
});

export function links() {
  return [{ rel: 'stylesheet', href: tailwind }];
}

export default function App() {
  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      </head>
      <body>
        <main className="relative flex min-h-screen w-full flex-col bg-zinc-100">
          <div className="sticky top-0 left-0 z-10 w-full bg-zinc-100">
            <Navbar />
          </div>
          <div className="h-full w-full px-8">
            <Outlet />
          </div>
        </main>
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
