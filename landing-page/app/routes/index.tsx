import { Link } from '@remix-run/react';
import Lottie from 'lottie-react';
import carAnimation from '../assets/animation/car_animation.json';

export default () => {
  return (
    <div className="flex h-[70vh] flex-row flex-wrap items-center justify-center md:space-x-32">
      <div className="flex flex-col space-y-2">
        <h3 className="text-5xl font-semibold text-gray-600">Ride in style</h3>
        <p className="pb-10 text-lg text-gray-600">Rent a Citybubble and enjoy the ride</p>
        <Link
          to="/booking"
          className="bg-white-200 rounded-xl bg-green-600 p-4 px-10 text-lg uppercase text-white shadow-xl hover:bg-green-700"
        >
          rent a citybubble now
        </Link>
      </div>
      <div className="">
        <Lottie className="h-[500px]" autoPlay loop animationData={carAnimation} />
      </div>
    </div>
  );
};
