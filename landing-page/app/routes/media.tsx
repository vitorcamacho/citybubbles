import api from '~/api/contentful';
import type { LoaderFunction } from '@remix-run/node';
import { useLoaderData } from '@remix-run/react';
import { Fragment, useState } from 'react';
import { Dialog, Transition } from '@headlessui/react';

type LoaderData = {
  media: Awaited<ReturnType<typeof api.Gallery>>;
};

export const loader: LoaderFunction = async () => {
  console.log('loader!!!');
  const media = await api.Gallery();

  return { media };
};

export default () => {
  const { media } = useLoaderData() as LoaderData;
  let [isOpen, setIsOpen] = useState(false);
  const [fullScreenImage, setFullScreenImage] = useState<string | null>(null);

  function closeModal() {
    setIsOpen(false);
    setFullScreenImage(null);
  }

  function openModal(image: string) {
    setFullScreenImage(image);
    setIsOpen(true);
  }

  return (
    <div className="mx-auto flex w-full flex-row flex-wrap items-center justify-center gap-4 py-4 md:p-4">
      <Transition appear show={isOpen} as={Fragment}>
        <Dialog as="div" className="relative z-10" onClose={closeModal}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-75" />
          </Transition.Child>
          <div className="fixed inset-0 h-full w-full overflow-y-auto">
            <div className="flex h-full w-full items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="relative transform rounded-2xl bg-white text-left align-middle shadow-xl transition-all">
                  <button
                    onClick={closeModal}
                    className="absolute -right-4 -top-4 z-10 rounded-full bg-white py-2 px-4 font-bold hover:bg-gray-200"
                  >
                    X
                  </button>
                  <img
                    alt={fullScreenImage as string}
                    src={fullScreenImage as string}
                    className="my-auto mx-auto overflow-hidden rounded"
                  />
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
      {media.galleryCollection?.items.map((image) => (
        <button
          key={image?.src?.url}
          type="button"
          onClick={() => openModal(image?.src?.url as string)}
        >
          <img
            alt={image?.src?.title ?? ''}
            src={image?.src?.url ?? ''}
            className="
              aspect-auto
              transform
              rounded-md
              object-cover
              shadow-sm
              transition
              ease-in-out
              hover:scale-105
              hover:opacity-50
              md:h-[300px]
              md:w-[300px]
              lg:h-[400px]
              lg:w-[400px]
            "
          />
        </button>
      ))}
    </div>
  );
};
